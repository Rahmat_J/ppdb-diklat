<?php

namespace Myth\Auth\Models;

use CodeIgniter\Database\MySQLi\Builder;
use CodeIgniter\Model;
use Myth\Auth\Authorization\GroupModel;
use Myth\Auth\Entities\User;

class UserModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $returnType = User::class;
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'email', 'username', 'nama', 'tglLhr', 'tempatLhr', 'jk', 'umur', 'email', 'telp2', 'telp_wa', 'alamat', 'kelurahan', 'kecamatan', 'wilayah', 'provinsi', 'ijasah', 'keilmuan', 'instansi', 'nama_instansi', 'bidang', 'jabatan', 'tglaktif', 'alamat_instansi', 'prov', 'kodepos', 'telp', 'email_instansi', 'password_hash', 'reset_hash', 'reset_at', 'reset_expires', 'activate_hash',
        'status', 'status_message', 'active', 'force_pass_reset', 'permissions', 'deleted_at',
    ];

    protected $useTimestamps = true;

    protected $validationRules = [
        'email'         => 'required|valid_email|is_unique[users.email,id,{id}]',
        'username'      => 'required|alpha_numeric_punct|min_length[3]|max_length[30]|is_unique[users.username,id,{id}]',
        'password_hash' => 'required',
    ];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $afterInsert = ['addToGroup'];

    /**
     * The id of a group to assign.
     * Set internally by withGroup.
     *
     * @var int|null
     */
    protected $assignGroup;

    /**
     * Logs a password reset attempt for posterity sake.
     *
     * @param string      $email
     * @param string|null $token
     * @param string|null $ipAddress
     * @param string|null $userAgent
     */
    public function logResetAttempt(string $email, string $token = null, string $ipAddress = null, string $userAgent = null)
    {
        $this->db->table('auth_reset_attempts')->insert([
            'email' => $email,
            'ip_address' => $ipAddress,
            'user_agent' => $userAgent,
            'token' => $token,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * Logs an activation attempt for posterity sake.
     *
     * @param string|null $token
     * @param string|null $ipAddress
     * @param string|null $userAgent
     */
    public function logActivationAttempt(string $token = null, string $ipAddress = null, string $userAgent = null)
    {
        $this->db->table('auth_activation_attempts')->insert([
            'ip_address' => $ipAddress,
            'user_agent' => $userAgent,
            'token' => $token,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * Sets the group to assign any users created.
     *
     * @param string $groupName
     *
     * @return $this
     */
    public function withGroup(string $groupName)
    {
        $group = $this->db->table('auth_groups')->where('name', $groupName)->get()->getFirstRow();

        $this->assignGroup = $group->id;

        return $this;
    }

    /**
     * Clears the group to assign to newly created users.
     *
     * @return $this
     */
    public function clearGroup()
    {
        $this->assignGroup = null;

        return $this;
    }

    /**
     * If a default role is assigned in Config\Auth, will
     * add this user to that group. Will do nothing
     * if the group cannot be found.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    protected function addToGroup($data)
    {
        if (is_numeric($this->assignGroup)) {
            $groupModel = model(GroupModel::class);
            $groupModel->addUserToGroup($data['id'], $this->assignGroup);
        }

        return $data;
    }

    /** 
     * Mencari data yang sama dengan inputan dari tabel users
     * 
     * @param string  $keyword
     * 
     * @return array
     */
    public function search($keyword)
    {
        return $this->table('users')->like('nama', $keyword)->orLike('id', $keyword);
    }

    /** 
     * Mencari data yang sama dengan inputan dari tabel users
     * 
     * @param string  $keyword
     * 
     * @return mixed
     */
    public function searchUser($keyword)
    {
        $builder = $this->table('users');
        $builder->select('*');
        $builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
        $builder->where('auth_groups_users.group_id', 2);
        $builder->like('nama', $keyword);
        $builder->orLike('id', $keyword);

        return $builder;
    }

    /** 
     * Mengambil semua data user kecuali admin dengan pagination
     * 
     * @param int  $id
     * 
     * @return object
     */
    public function getUserPaginate($nb, $tablename, $data = null)
    {
        if ($data == null) {
            $builder = $this->table('users');
            $builder->select('*');
            $builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
            $builder->where('auth_groups_users.group_id', 2);
            $builder->orderBy('nama');

            return $builder->paginate($nb, $tablename);
        } else {
            return $data->paginate($nb, $tablename);
        }
    }

    /** 
     * Mengambil semua data user kecuali admin 
     * 
     * @param int  $id
     * 
     * @return object
     */
    public function getUser()
    {

        $builder = $this->table('users');
        $builder->select('*');
        $builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
        $builder->where('auth_groups_users.group_id', 2);
        $builder->orderBy('nama');

        return $builder->get()->getResult();
    }


    /** 
     * Mengambil pelatihan yang diikuti user
     * 
     * @return object
     * 
     */
    public function getPelatihanFromUser($userid)
    {
        $builder = $this->table('users');
        $builder->select('pelatihan.nama as namapelatihan,pelatihan.id as pelatihanid');
        $builder->join('users_pelatihan', 'users_pelatihan.user_id = users.id');
        $builder->where('users.id', $userid);
        $builder->join('pelatihan', 'pelatihan.id = users_pelatihan.pelatihan_id');
        $query = $builder->get()->getResult();
        $pelatihan = array_column($query, 'namapelatihan', 'pelatihanid');
        return $pelatihan;
    }

    /** 
     * Mengambil semua user yang ingin mengikuti pelatihan
     * 
     * @return object
     * 
     */
    public function getSeleksi($id)
    {

        $builder = $this->table('users');
        $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, tglLhr, jk, ijasah, keilmuan, nama_instansi, seleksi.id as seleksiid, nama_file');
        $builder->join('seleksi', 'seleksi.user_id = users.id');
        $builder->join('pelatihan', 'pelatihan.id = seleksi.pelatihan_id');
        $builder->where('pelatihan.id', $id);
        $query = $builder->get()->getResult();

        return $query;
    }

    /** 
     * Mengambil semua user yang ingin mengikuti pelatihan
     * 
     * @return object
     * 
     */
    public function searchSeleksi($keyword)
    {
        $builder = $this->table('users');
        $builder->select('users.id as userid, users.nama as namauser, pelatihan.nama as namapelatihan, pelatihan.id as pelatihanid, tglLhr, jk, ijasah, keilmuan, instansi');
        $builder->join('seleksi', 'seleksi.user_id = users.id');
        $builder->join('pelatihan', 'pelatihan.id = seleksi.pelatihan_id');
        $builder->like('pelatihan.nama', $keyword);
        $builder->orLike('users.nama', $keyword);

        return $builder;
    }

    /** 
     * Mengambil semua user yang ingin direkap
     * 
     * @return mixed
     * 
     */
    public function getRekap($pelatihanid)
    {

        $builder = $this->table('users');
        $builder->select('users.id as userid, users.nama as namauser, pelatihan.nama as namapelatihan, pelatihan.id as pelatihanid, tglLhr, jk, ijasah, keilmuan, status.status as lolos, nama_instansi, tempatLhr, tglLhr, alamat, kelurahan, kecamatan, wilayah, provinsi, telp2, email, jabatan, bidang, tglaktif, alamat_instansi, telp, email_instansi, prov, biaya, tglAwal, tglAkhir');
        $builder->join('status', 'status.user_id = users.id');
        $builder->where('pelatihan.id', $pelatihanid);
        $builder->join('pelatihan', 'pelatihan.id = status.pelatihan_id');
        $query = $builder->get()->getResult();

        return $query;
    }

    // /** 
    //  * Mengambil semua user sudah membayar
    //  * 
    //  * @return object
    //  * 
    //  */
    // public function getBukti($id)
    // {

    //     $builder = $this->table('users');
    //     $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, bukti.id as buktiid, nominal, bukti_image, email, telp2, nama_instansi');
    //     $builder->join('bukti', 'bukti.user_id = users.id');
    //     $builder->join('pelatihan', 'pelatihan.id = bukti.pelatihan_id');
    //     $builder->where('pelatihan.id', $id);
    //     $query = $builder->get()->getResult();

    //     return $query;
    // }

    /** 
     * Mengambil semua user sudah membayar
     * 
     * @return mixed
     * 
     */
    public function searchBukti($id, $keyword)
    {

        $builder = $this->table('users');
        $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, bukti.id as buktiid, nominal, bukti_image, email, telp2, nama_instansi');
        $builder->join('bukti', 'bukti.user_id = users.id');
        $builder->join('pelatihan', 'pelatihan.id = bukti.pelatihan_id');
        $builder->where('pelatihan.id', $id);
        $builder->like('users.nama', $keyword);


        return $builder;
    }

    /** 
     * Mengambil semua user sudah membayar
     * 
     * @return object
     * 
     */
    public function getBukti($id, $data = null)
    {
        if ($data == null) {
            $builder = $this->table('users');
            $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, bukti.id as buktiid, nominal, bukti_image, email, telp2, nama_instansi');
            $builder->join('bukti', 'bukti.user_id = users.id');
            $builder->join('pelatihan', 'pelatihan.id = bukti.pelatihan_id');
            $builder->where('pelatihan.id', $id);


            return $builder->get()->getResult();
        } else {
            return $data->get()->getResult();
        }
    }

    /** 
     * Mengambil semua user yang ingin lolos seleksi
     * 
     * @return mixed
     * 
     */
    public function getPesertaLolos($id)
    {

        $builder = $this->table('users');
        $builder->select('users.id as userid, users.nama as namauser, pelatihan.nama as namapelatihan, pelatihan.id as pelatihanid');
        $builder->join('status', 'status.user_id = users.id');
        $builder->join('pelatihan', 'pelatihan.id = status.pelatihan_id');
        $builder->where('status.status', 1);
        $builder->where('pelatihan.id', $id);
        $query = $builder->get()->getResult();
        $users = array_column($query, 'namauser', 'userid');

        return $users;
    }

    /** 
     * Mengambil semua user yang ingin mengikuti pelatihan
     * 
     * @return object
     * 
     */
    public function getExportBukti($id)
    {

        $builder = $this->table('users');
        $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, bukti.id as buktiid, pelatihan.nama as namapelatihan, nominal, bukti_image, tglLhr, jk, ijasah, keilmuan, nama_instansi, tempatLhr, tglLhr, alamat, kelurahan, kecamatan, wilayah, provinsi, telp2, email, jabatan, bidang, tglaktif, alamat_instansi, telp, email_instansi, prov, biaya, tglAwal, tglAkhir');
        $builder->join('bukti', 'bukti.user_id = users.id');
        $builder->join('pelatihan', 'pelatihan.id = bukti.pelatihan_id');
        $builder->where('pelatihan.id', $id);
        $query = $builder->get()->getResult();


        return $query;
    }

    // /** 
    //  * Mengambil semua user yang mempunyai sertifikat
    //  * 
    //  * @return object
    //  * 
    //  */
    // public function getSertifikat($id)
    // {
    //     $builder = $this->table('users');
    //     $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, sertifikat.id as sertifikatid, nama_file');
    //     $builder->join('sertifikat', 'sertifikat.user_id = users.id');
    //     $builder->join('pelatihan', 'pelatihan.id = sertifikat.pelatihan_id');
    //     $builder->where('pelatihan.id', $id);
    //     $query = $builder->get()->getResult();

    //     return $query;
    // }

    /** 
     * Fitur search semua user yang mempunyai sertifikat berdasarkan keyword
     * 
     * @return object
     * 
     */
    public function searchSertifikat($id, $keyword)
    {
        $builder = $this->table('users');
        $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, sertifikat.id as sertifikatid, nama_file');
        $builder->join('sertifikat', 'sertifikat.user_id = users.id');
        $builder->join('pelatihan', 'pelatihan.id = sertifikat.pelatihan_id');
        $builder->where('pelatihan.id', $id);
        $builder->like('users.nama', $keyword);

        return $builder;
    }

    /** 
     * Mengambil semua user yang mempunyai sertifikat
     * 
     * @return object
     * 
     */
    public function getSertifikat($id, $data = null)
    {
        if ($data == null) {
            $builder = $this->table('users');
            $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, sertifikat.id as sertifikatid, nama_file');
            $builder->join('sertifikat', 'sertifikat.user_id = users.id');
            $builder->join('pelatihan', 'pelatihan.id = sertifikat.pelatihan_id');
            $builder->where('pelatihan.id', $id);

            return $builder->get()->getResult();
        } else {
            return $data->get()->getResult();
        }
    }

    /** 
     * Mengambil semua user yang mempunyai sertifikat
     * 
     * @return object
     * 
     */
    public function getUserFromBukti($id)
    {
        $builder = $this->table('users');
        $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, pelatihan.nama as namapelatihan');
        $builder->join('bukti', 'bukti.user_id = users.id');
        $builder->join('pelatihan', 'pelatihan.id = bukti.pelatihan_id');
        $builder->where('pelatihan.id', $id);
        $query = $builder->get()->getResult();
        $users = array_column($query, 'namauser', 'userid');

        return $users;
    }

    /** 
     * Mengambil semua data dari tabel users yang sudah mempunyai sertifikat tetapi menggunakan fungsi pagination untuk page nya untuk satu user
     * 
     * @param $userid
     * @param $nb data per page yang diinginkan
     * @param $tablename nama tabel yang ingin dipage
     * 
     * @return object
     * 
     */
    public function getUserSertifikat($userid, $nb, $tablename, $data = null)
    {
        if ($data == null) {

            $builder = $this->table('users');
            $builder->select('users.id as userid, users.nama as namauser, pelatihan.id as pelatihanid, sertifikat.id as sertifikatid, nama_file, pelatihan.nama as namapelatihan, tglAwal, tglAkhir');
            $builder->join('sertifikat', 'sertifikat.user_id = users.id');
            $builder->join('pelatihan', 'pelatihan.id = sertifikat.pelatihan_id');
            $builder->where('users.id', $userid);

            return $builder->paginate($nb, $tablename);
        } else {
            return $data->paginate($nb, $tablename);
        }
    }

    /** 
     * Mengambil satu atau beberapa data dari tabel users yang sudah mempunyai sertifikat yang menyerupai keyword
     * 
     * @param $keyword
     * @param $userid
     * 
     * @return object
     * 
     */
    public function searchSertifikatUser($keyword, $userid)
    {
        $builder = $this->table('users');
        $builder->select('users.id as userid, users.nama as namauser, nama_file, pelatihan.nama as namapelatihan, sertifikat.id as sertifikatid, tglAwal, tglAkhir');
        $builder->join('sertifikat', 'sertifikat.user_id = users.id');
        $builder->join('pelatihan', 'pelatihan.id = sertifikat.pelatihan_id');
        $builder->where('users.id', $userid);
        $builder->like('pelatihan.nama', $keyword);

        return $builder;
    }
}
