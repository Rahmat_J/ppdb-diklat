<?php

namespace Myth\Auth\Test\Fakers;

use Faker\Generator;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;
use Faker;

class UserFaker extends UserModel
{
	/**
	 * Faked data for Fabricator.
	 *
	 * @param Generator $faker
	 *
	 * @return User
	 */
	public function fake(Generator &$faker): User
	{
		$faker = Faker\Factory::create('id_ID');
		return new User([
			'email'    => $faker->email,
			'username' => $faker->userName,
			'nama' => $faker->name,
			'active' => 1,
			'password' => bin2hex(random_bytes(16)),
		]);
	}
}
