-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2022 at 06:00 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_activation_attempts`
--

CREATE TABLE `auth_activation_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups`
--

CREATE TABLE `auth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_groups`
--

INSERT INTO `auth_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', ''),
(2, 'user', '');

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_permissions`
--

CREATE TABLE `auth_groups_permissions` (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_users`
--

CREATE TABLE `auth_groups_users` (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_groups_users`
--

INSERT INTO `auth_groups_users` (`group_id`, `user_id`) VALUES
(1, 1),
(2, 2),
(2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `auth_logins`
--

CREATE TABLE `auth_logins` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `date` datetime NOT NULL,
  `success` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_logins`
--

INSERT INTO `auth_logins` (`id`, `ip_address`, `email`, `user_id`, `date`, `success`) VALUES
(1, '::1', 'admin@gmail.com', 1, '2022-04-25 00:44:37', 1),
(2, '::1', 'admin@gmail.com', 1, '2022-04-25 00:45:31', 1),
(3, '::1', 'admin@gmail.com', 1, '2022-04-25 00:46:22', 1),
(4, '::1', 'admin@gmail.com', 1, '2022-04-25 00:47:06', 1),
(5, '::1', 'admin@gmail.com', 1, '2022-04-25 00:49:01', 1),
(6, '::1', 'fauzi@gmail.com', 3, '2022-04-25 01:00:37', 1),
(7, '::1', 'admin@gmail.com', 1, '2022-04-25 06:22:44', 1),
(8, '::1', 'admin', NULL, '2022-04-25 09:24:28', 0),
(9, '::1', 'admin@gmail.com', 1, '2022-04-25 09:24:33', 1),
(10, '::1', 'admin@gmail.com', 1, '2022-04-25 09:25:20', 1),
(11, '::1', 'amedfg@gmail.com', 2, '2022-04-25 15:57:32', 1),
(12, '::1', 'amedfg@gmail.com', 2, '2022-04-25 15:59:03', 1),
(13, '::1', 'amedfg@gmail.com', 2, '2022-04-25 15:59:27', 1),
(14, '::1', 'admin@gmail.com', 1, '2022-04-25 16:16:33', 1),
(15, '::1', 'fauzi@gmail.com', 3, '2022-04-25 16:17:25', 1),
(16, '::1', 'admin@gmail.com', 1, '2022-04-26 22:46:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_permissions`
--

CREATE TABLE `auth_permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_permissions`
--

INSERT INTO `auth_permissions` (`id`, `name`, `description`) VALUES
(1, 'upload_bukti', 'Filter untuk peserta yang sudah lolos seleksi dan bisa mengupload bukti pembayaran'),
(2, 'sudah_upload_bukti', 'Filter untuk peserta yang sudah mengupload bukti bayar'),
(3, 'pilih_pelatihan', 'Filter untuk peserta yang sudah memilih pelatihan');

-- --------------------------------------------------------

--
-- Table structure for table `auth_reset_attempts`
--

CREATE TABLE `auth_reset_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_tokens`
--

CREATE TABLE `auth_tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `selector` varchar(255) NOT NULL,
  `hashedValidator` varchar(255) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_users_permissions`
--

CREATE TABLE `auth_users_permissions` (
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_users_permissions`
--

INSERT INTO `auth_users_permissions` (`user_id`, `permission_id`) VALUES
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bukti`
--

CREATE TABLE `bukti` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `pelatihan_id` int(11) UNSIGNED NOT NULL,
  `nominal` int(20) DEFAULT NULL,
  `bukti_image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2017-11-20-223112', 'Myth\\Auth\\Database\\Migrations\\CreateAuthTables', 'default', 'Myth\\Auth', 1650865067, 1),
(2, '2022-04-25-053452', 'App\\Database\\Migrations\\Pelatihan', 'default', 'App', 1650865067, 1),
(3, '2022-04-25-053459', 'App\\Database\\Migrations\\Seleksi', 'default', 'App', 1650865067, 1),
(4, '2022-04-25-053504', 'App\\Database\\Migrations\\Status', 'default', 'App', 1650865067, 1),
(5, '2022-04-25-053524', 'App\\Database\\Migrations\\BuktiBayar', 'default', 'App', 1650865067, 1),
(6, '2022-04-25-053534', 'App\\Database\\Migrations\\Sertifikat', 'default', 'App', 1650865067, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pelatihan`
--

CREATE TABLE `pelatihan` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `tglAwal` date DEFAULT NULL,
  `tglAkhir` date DEFAULT NULL,
  `jumlah_peserta` varchar(50) DEFAULT NULL,
  `tempat` varchar(255) DEFAULT NULL,
  `penyelenggara` varchar(255) DEFAULT NULL,
  `skp_id` char(50) DEFAULT NULL,
  `biaya` int(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pelatihan`
--

INSERT INTO `pelatihan` (`id`, `nama`, `kategori`, `tglAwal`, `tglAkhir`, `jumlah_peserta`, `tempat`, `penyelenggara`, `skp_id`, `biaya`, `created_at`, `updated_at`) VALUES
(1, 'Manajemen Risiko', 'NON TEKNIS', '2022-04-20', '2022-04-23', '23', 'Online', 'PMI Pusat', '', 300000, '2022-04-25 01:11:37', '2022-04-25 01:11:37'),
(2, 'Rekrutmen dan Seleksi Donor Plasma Konvalesen', 'TEKNIS', '2022-05-10', '2022-05-15', '20', 'ONLINE', 'PMI PUSAT', '', 500000, '2022-04-25 01:15:49', '2022-04-25 06:24:20');

-- --------------------------------------------------------

--
-- Table structure for table `seleksi`
--

CREATE TABLE `seleksi` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `pelatihan_id` int(11) UNSIGNED NOT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `motivasi` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sertifikat`
--

CREATE TABLE `sertifikat` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `pelatihan_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `nama_file` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `pelatihan_id` int(11) UNSIGNED NOT NULL,
  `status` tinyint(1) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `noKTP` varchar(35) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `jk` char(1) DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `tempatLhr` varchar(50) DEFAULT NULL,
  `tglLhr` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `golDarah` char(2) DEFAULT NULL,
  `rhesus` varchar(9) DEFAULT NULL,
  `kelurahan` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `wilayah` varchar(100) DEFAULT NULL,
  `provinsi` varchar(100) DEFAULT NULL,
  `jumdik` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `telp2` varchar(100) DEFAULT NULL,
  `telp_wa` varchar(100) DEFAULT NULL,
  `umur` int(11) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `instansi` varchar(100) DEFAULT NULL,
  `nama_instansi` varchar(255) DEFAULT NULL,
  `alamat_instansi` varchar(255) DEFAULT NULL,
  `kodepos` varchar(100) DEFAULT NULL,
  `prov` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `ijasah` varchar(100) DEFAULT NULL,
  `keilmuan` varchar(100) DEFAULT NULL,
  `tglaktif` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `email_instansi` varchar(255) NOT NULL,
  `bidang` varchar(100) DEFAULT NULL,
  `user_image` varchar(100) NOT NULL DEFAULT 'default.png',
  `password_hash` varchar(255) NOT NULL,
  `reset_hash` varchar(255) DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `reset_expires` datetime DEFAULT NULL,
  `activate_hash` varchar(255) DEFAULT NULL,
  `status_message` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `force_pass_reset` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `noKTP`, `nama`, `alamat`, `jk`, `telp`, `tempatLhr`, `tglLhr`, `status`, `golDarah`, `rhesus`, `kelurahan`, `kecamatan`, `wilayah`, `provinsi`, `jumdik`, `title`, `telp2`, `telp_wa`, `umur`, `username`, `updated_at`, `instansi`, `nama_instansi`, `alamat_instansi`, `kodepos`, `prov`, `created_at`, `jabatan`, `ijasah`, `keilmuan`, `tglaktif`, `email`, `email_instansi`, `bidang`, `user_image`, `password_hash`, `reset_hash`, `reset_at`, `reset_expires`, `activate_hash`, `status_message`, `active`, `force_pass_reset`, `deleted_at`) VALUES
(1, NULL, 'ADMIN', '', NULL, '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', '', NULL, NULL, '', '', NULL, 'admin', '2022-04-25 00:44:11', NULL, '', '', '', '', '2022-04-25 00:44:11', '', NULL, '', '', 'admin@gmail.com', '', '', 'default.png', '$2y$10$N5YG6l1du66mCMAzuiJg4.LnwdImBs18.XcuAuTLUfB9rX8JWQR5G', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL),
(2, NULL, 'Achmad Fauzi Guritno', 'Jalan sadar no 6', '0', '0210303012', 'Jakarta', '2002-01-23', NULL, NULL, NULL, 'Ciganjur', 'Jagakarsa', 'Jakarta Selatan', 'DKI Jakarta', NULL, NULL, '08282828190', '08282828190', 0, 'amedfg', '2022-04-25 16:05:58', 'UDD', 'UDDP PMI', 'Jalan Joe No 7', '12630', 'Jakarta Selatan', '2022-04-25 00:54:22', 'Kepala', 'S1', 'Informatika', '2008-06-10', 'amedfg@gmail.com', 'uddppmi@gmail.com', 'IT', 'default.png', '$2y$10$65XkJRd5Nee.HQk8nPiuKep2sf9j7vH4s06C5GXHaLsY.CIJ6ueWe', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL),
(3, NULL, 'Fauzi', 'Jalan Joe', '0', '921921991', 'Jakarta', '2000-02-01', NULL, NULL, NULL, 'Lenteng Agung', 'Jagakarsa', 'Jakarta Selatan', 'DKI Jakarta', NULL, NULL, '091329312312', '', NULL, 'achmadfg', '2022-04-25 00:56:40', 'UDD', 'UDD PMI JAKARTA SELATAN', 'Jalan Joe no 7', '123949', 'Jakarta Selatan', '2022-04-25 00:56:40', 'Kepala', 'S1', 'Informatika', '2001-02-14', 'fauzi@gmail.com', 'amed@gmail.com', 'Keuangan', 'default.png', '$2y$10$fFQINemA8Ue.BmLL5IMrTOyXBBmRlj8CDs/vdFl6zR1lZh65P.Ode', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups`
--
ALTER TABLE `auth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD KEY `auth_groups_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `group_id_permission_id` (`group_id`,`permission_id`);

--
-- Indexes for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD KEY `auth_groups_users_user_id_foreign` (`user_id`),
  ADD KEY `group_id_user_id` (`group_id`,`user_id`);

--
-- Indexes for table `auth_logins`
--
ALTER TABLE `auth_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_tokens_user_id_foreign` (`user_id`),
  ADD KEY `selector` (`selector`);

--
-- Indexes for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD KEY `auth_users_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `user_id_permission_id` (`user_id`,`permission_id`);

--
-- Indexes for table `bukti`
--
ALTER TABLE `bukti`
  ADD KEY `bukti_user_id_foreign` (`user_id`),
  ADD KEY `bukti_pelatihan_id_foreign` (`pelatihan_id`),
  ADD KEY `id_user_id_pelatihan_id` (`id`,`user_id`,`pelatihan_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelatihan`
--
ALTER TABLE `pelatihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seleksi`
--
ALTER TABLE `seleksi`
  ADD KEY `seleksi_user_id_foreign` (`user_id`),
  ADD KEY `seleksi_pelatihan_id_foreign` (`pelatihan_id`),
  ADD KEY `id_user_id_pelatihan_id` (`id`,`user_id`,`pelatihan_id`);

--
-- Indexes for table `sertifikat`
--
ALTER TABLE `sertifikat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sertifikat_pelatihan_id_foreign` (`pelatihan_id`),
  ADD KEY `user_id_pelatihan_id` (`user_id`,`pelatihan_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD KEY `status_user_id_foreign` (`user_id`),
  ADD KEY `status_pelatihan_id_foreign` (`pelatihan_id`),
  ADD KEY `id_user_id_pelatihan_id` (`id`,`user_id`,`pelatihan_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_groups`
--
ALTER TABLE `auth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth_logins`
--
ALTER TABLE `auth_logins`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bukti`
--
ALTER TABLE `bukti`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pelatihan`
--
ALTER TABLE `pelatihan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `seleksi`
--
ALTER TABLE `seleksi`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sertifikat`
--
ALTER TABLE `sertifikat`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD CONSTRAINT `auth_groups_permissions_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD CONSTRAINT `auth_groups_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD CONSTRAINT `auth_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD CONSTRAINT `auth_users_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_users_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bukti`
--
ALTER TABLE `bukti`
  ADD CONSTRAINT `bukti_pelatihan_id_foreign` FOREIGN KEY (`pelatihan_id`) REFERENCES `pelatihan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bukti_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `seleksi`
--
ALTER TABLE `seleksi`
  ADD CONSTRAINT `seleksi_pelatihan_id_foreign` FOREIGN KEY (`pelatihan_id`) REFERENCES `pelatihan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `seleksi_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sertifikat`
--
ALTER TABLE `sertifikat`
  ADD CONSTRAINT `sertifikat_pelatihan_id_foreign` FOREIGN KEY (`pelatihan_id`) REFERENCES `pelatihan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sertifikat_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `status`
--
ALTER TABLE `status`
  ADD CONSTRAINT `status_pelatihan_id_foreign` FOREIGN KEY (`pelatihan_id`) REFERENCES `pelatihan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `status_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
