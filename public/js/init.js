$(function() {
    //Date picker
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose:true,
    });
});

function previewImg() {

    const foto = document.querySelector('#foto');
    const fotoLabel = document.querySelector('.custom-file-label');
    const imgPreview = document.querySelector('.img-preview');

    fotoLabel.textContent = foto.files[0].name;

    const filefoto = new FileReader();
    filefoto.readAsDataURL(foto.files[0]);

    filefoto.onload = function(e) {
        imgPreview.src = e.target.result;
    }
}

function previewDocs(){
    const file = document.querySelector('#file');
    const fileLabel = document.querySelector('.custom-file-label');
    fileLabel.textContent = file.files[0].name;

}