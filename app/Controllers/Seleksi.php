<?php

namespace App\Controllers;

use \Myth\Auth\Models\UserModel;
use \App\Models\PelatihanModel;
use \App\Models\SeleksiModel;
use \App\Models\StatusModel;

class Seleksi extends BaseController
{
    protected $userModel, $pelatihanModel, $seleksiModel, $statusModel;

    public function __construct()
    {
        $this->db      = \Config\Database::connect();
        $this->userModel = new UserModel();
        $this->pelatihanModel = new PelatihanModel();
        $this->seleksiModel = new SeleksiModel();
        $this->statusModel = new StatusModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page_seleksi') ? $this->request->getVar('page_seleksi') : 1;
        $data['currentPage'] = $currentPage;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $pelatihan = $this->pelatihanModel->search($keyword);
        } else {
            $pelatihan = $this->pelatihanModel;
        }

        $data['title'] = 'Seleksi Peserta';
        $data['pelatihan'] = $this->pelatihanModel->paginate(5, 'seleksi');
        $data['pager'] = $this->pelatihanModel->pager;
        return view('admin/seleksi/index', $data);
    }

    public function peserta($id)
    {
        $data['pelatihan'] = $this->pelatihanModel->find($id);
        $data['title'] = 'Daftar Peserta yang ingin diseleksi';
        $data['users'] = $this->userModel->getSeleksi($id);
        return view('admin/seleksi/peserta', $data);
    }

    public function add()
    {
        $data['title'] = 'Tambah Peserta yang diSeleksi';
        $data['users'] = $this->userModel->getUser();
        $data['pelatihan'] = $this->pelatihanModel->orderBy("tglAwal", "Desc")->findAll();

        return view('admin/seleksi/create', $data);
    }

    public function save()
    {
        $rules = [
            'peserta' => 'required',
            'pelatihan' => 'required',
            'nama_file' => 'uploaded[nama_file]|max_size[nama_file,4096]|mime_in[nama_file,application/pdf]',
        ];

        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //ambil Surat
        $fileSurat = $this->request->getFile('nama_file');
        //ambil nama Surat
        $namaSurat = $fileSurat->getRandomName();

        //pindahkan file ke Surat
        $fileSurat->move('surat_keterangan_aktif/', $namaSurat);

        //save ke database
        $this->seleksiModel->save([
            'user_id' => $this->request->getVar('peserta'),
            'pelatihan_id' => $this->request->getVar('pelatihan'),
            'nama_file' => $namaSurat,
        ]);

        return redirect()->to('seleksi')->with('message', 'Berhasil menambah peserta');
    }

    public function tolak($seleksiid)
    {
        $auth = service('authorization');
        $seleksi = $this->seleksiModel->find($seleksiid);
        $peserta = $this->seleksiModel->getSeleksi($seleksiid);
        $pelatihan = $this->pelatihanModel->find($peserta['pelatihan_id']);

        //masukkan ke tabel status
        $this->statusModel->save([
            'user_id' => $peserta['user_id'],
            'pelatihan_id' => $peserta['pelatihan_id'],
            'status' => 0,
        ]);

        //hapus surat lama
        unlink('surat_keterangan_aktif/' . $seleksi['nama_file']);

        //hapus dari tabel seleksi
        $this->seleksiModel->delete($seleksiid);

        $auth->removePermissionFromUser('pilih_pelatihan', $peserta['user_id']);

        return redirect()->to('seleksi/peserta/' . $pelatihan['id'])->with('warning', 'Berhasil menolak Peserta');
    }

    public function terima($seleksiid)
    {
        $auth = service('authorization');
        $seleksi = $this->seleksiModel->find($seleksiid);
        $peserta = $this->seleksiModel->getSeleksi($seleksiid);
        $pelatihan = $this->pelatihanModel->find($peserta['pelatihan_id']);

        //masukkan ke tabel status
        $this->statusModel->save([
            'user_id' => $peserta['user_id'],
            'pelatihan_id' => $peserta['pelatihan_id'],
            'status' => 1,
        ]);

        //hapus surat lama
        unlink('surat_keterangan_aktif/' . $seleksi['nama_file']);

        //hapus dari tabel seleksi
        $this->seleksiModel->delete($seleksiid);

        $auth->removePermissionFromUser('pilih_pelatihan', $peserta['user_id']);

        $auth->addPermissionToUser('upload_bukti', $peserta['user_id']);

        return redirect()->to('seleksi/peserta/' . $pelatihan['id'])->with('message', 'Berhasil menerima Peserta');
    }
}
