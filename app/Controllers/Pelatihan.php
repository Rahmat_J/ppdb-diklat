<?php

namespace App\Controllers;

use \App\Models\PelatihanModel;


class Pelatihan extends BaseController
{
    protected $pelatihanModel;

    public function __construct()
    {
        $this->pelatihanModel = new PelatihanModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page_pelatihan') ? $this->request->getVar('page_pelatihan') : 1;
        $data['currentPage'] = $currentPage;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $pelatihan = $this->pelatihanModel->search($keyword);
        } else {
            $pelatihan = $this->pelatihanModel;
        }

        $data['title'] = 'Daftar Pelatihan';
        $data['pelatihan'] = $this->pelatihanModel->orderBy("tglAwal", "Desc")->paginate(5, 'pelatihan');
        $data['pager'] = $this->pelatihanModel->pager;

        return view('admin/pelatihan/index', $data);
    }

    public function add()
    {
        $data = [
            'title' => 'Tambah Pelatihan',
        ];
        return view('admin/pelatihan/create', $data);
    }

    public function save()
    {
        $rules = [
            'nama'    => 'required',
            'kategori'    => 'required',
            'tglAwal'    => 'required',
            'tglAkhir'    => 'required',
            'jumlah_peserta'    => 'required',
            'tempat'    => 'required',
            'penyelenggara'    => 'required',
            'biaya'    => 'required',
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        $this->pelatihanModel->save([
            'nama' => $this->request->getVar('nama'),
            'kategori' => $this->request->getVar('kategori'),
            'tglAwal' => $this->request->getVar('tglAwal'),
            'tglAkhir' => $this->request->getVar('tglAkhir'),
            'jumlah_peserta' => $this->request->getVar('jumlah_peserta'),
            'tempat' => $this->request->getVar('tempat'),
            'penyelenggara' => $this->request->getVar('penyelenggara'),
            'skp_id' => $this->request->getVar('skp_id'),
            'biaya' => $this->request->getVar('biaya'),
        ]);

        return redirect()->to('pelatihan')->with('message', 'Data Berhasil Ditambahkan');
    }

    public function delete($id)
    {
        $this->pelatihanModel->delete($id);
        return redirect()->to('pelatihan')->with('warning', 'Data Berhasil Dihapus');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Form Mengubah Data',
            'pelatihan' => $this->pelatihanModel->getPelatihan($id)
        ];

        return view('admin/pelatihan/edit', $data);
    }

    public function update($id)
    {
        $rules = [
            'nama'    => 'required',
            'kategori'    => 'required',
            'tglAwal'    => 'required',
            'tglAkhir'    => 'required',
            'tempat'    => 'required',
            'penyelenggara'    => 'required',
            'jumlah_peserta'    => 'required',
            'biaya'    => 'required',
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        $this->pelatihanModel->save([
            'id' => $id,
            'nama' => $this->request->getVar('nama'),
            'kategori' => $this->request->getVar('kategori'),
            'tglAwal' => $this->request->getVar('tglAwal'),
            'tglAkhir' => $this->request->getVar('tglAkhir'),
            'jumlah_peserta' => $this->request->getVar('jumlah_peserta'),
            'tempat' => $this->request->getVar('tempat'),
            'penyelenggara' => $this->request->getVar('penyelenggara'),
            'skp_id' => $this->request->getVar('skp_id'),
            'biaya' => $this->request->getVar('biaya'),
        ]);

        return redirect()->to('pelatihan')->with('message', 'Data Berhasil Diubah');
    }
}
