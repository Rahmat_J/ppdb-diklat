<?php

namespace App\Controllers;

use Myth\Auth\Entities\User;
use \Myth\Auth\Models\UserModel;

class Admin extends BaseController
{

    protected $db, $builder, $userModel, $config, $authorize;

    public function __construct()
    {
        $this->db      = \Config\Database::connect();
        $this->builder = $this->db->table('users');
        $this->userModel = new UserModel();
        $this->config = config('Auth');
        $this->authorize = service('authorization');
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page_users') ? $this->request->getVar('page_users') : 1;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $user = $this->userModel->searchUser($keyword);
        } else {
            $user = $this->userModel;
        }

        $data = [
            'title' => 'User List',
            'users' => $this->userModel->getUserPaginate(5, 'users'),
            'pager' => $this->userModel->pager,
            'currentPage' => $currentPage
        ];
        return view('admin/peserta/index', $data);
    }

    public function detail($id)
    {
        $data['title'] = 'User Detail';
        $this->builder->select('*');
        $this->builder->where('users.id', $id);
        $query = $this->builder->get();

        $data['user'] = $query->getRow();
        return view('admin/peserta/detail', $data);
    }

    public function add()
    {
        $data['title'] = 'Tambah Peserta';
        return view('admin/peserta/create', $data);
    }

    public function save()
    {

        $users = model(UserModel::class);

        //validasi input
        $rules = [
            'username' => 'required|alpha_numeric_space|min_length[3]|max_length[30]|is_unique[users.username]',
            'email'    => 'required|valid_email|is_unique[users.email]',
            'nama'    => 'required',
            'jk'    => 'required',
            'tempatLhr'    => 'required',
            'tglLhr'    => 'required',
            'telp2'    => 'required',
            'alamat' => 'required',
            'kelurahan'    => 'required',
            'kecamatan'    => 'required',
            'wilayah'    => 'required',
            'provinsi'    => 'required',
            'ijasah'    => 'required',
            'keilmuan'    => 'required',
            'instansi'    => 'required',
            'nama_instansi'    => 'required',
            'alamat_instansi'    => 'required',
            'bidang'    => 'required',
            'tglaktif'    => 'required',
            'prov'    => 'required',
            'kodepos'    => 'required',
            'telp'    => 'required',
            'email_instansi'    => 'required',
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //validasi password
        $rules = [
            'password'     => 'required|strong_password',
            'pass_confirm' => 'required|matches[password]',
        ];

        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //save the user
        $allowedPostFields = array_merge(['password'], $this->config->validFields, $this->config->personalFields);
        $user = new User($this->request->getPost($allowedPostFields));

        $this->config->requireActivation === null ? $user->activate() : $user->generateActivateHash();


        // Ensure default group gets assigned if set
        if (!empty($this->config->defaultUserGroup)) {
            $users = $users->withGroup($this->config->defaultUserGroup);
        }

        if (!$users->save($user)) {
            return redirect()->back()->withInput()->with('errors', $users->errors());
        }

        return redirect()->to('admin')->with('message', 'Data Berhasil Ditambahkan');
    }

    public function delete($id)
    {
        $this->builder->delete(['id' => $id]);
        $this->authorize->removeUserFromGroup($id, 'user');
        return redirect()->to('admin')->with('warning', 'Data Berhasil Dihapus');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Form Mengubah Data',
            'user' => $this->userModel->find($id)
        ];

        return view('admin/peserta/edit', $data);
    }
    public function update($id)
    {
        $users = model(UserModel::class);
        $user = $this->userModel->find($id);
        //cek username
        $usernameLama = $this->userModel->find($id);
        if ($usernameLama->username == $this->request->getVar('username')) {
            $rule_username = 'required';
        } else {
            $rule_username = 'required|alpha_numeric_space|min_length[3]|max_length[30]|is_unique[users.username]';
        }


        $rules = [
            'username' => $rule_username,
            'nama'    => 'required',
            'jk'    => 'required',
            'tempatLhr'    => 'required',
            'tglLhr'    => 'required',
            'telp2'    => 'required',
            'alamat' => 'required',
            'kelurahan'    => 'required',
            'kecamatan'    => 'required',
            'wilayah'    => 'required',
            'provinsi'    => 'required',
            'ijasah'    => 'required',
            'keilmuan'    => 'required',
            'instansi'    => 'required',
            'nama_instansi'    => 'required',
            'alamat_instansi'    => 'required',
            'bidang'    => 'required',
            'tglaktif'    => 'required',
            'prov'    => 'required',
            'kodepos'    => 'required',
            'telp'    => 'required',
            'email_instansi'    => 'required',
        ];

        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //input umur
        $tanggalSkrg = date("Y-m-d");
        $tglLhr = $this->request->getPost('tglLhr');
        $age = date_diff(date_create($tglLhr), date_create($tanggalSkrg));

        //update ke database
        $user->username = $this->request->getPost('username');
        $user->nama = $this->request->getPost('nama');
        $user->tempatLhr = $this->request->getPost('tempatLhr');
        $user->jk = $this->request->getPost('jk');
        $user->tglLhr = $this->request->getPost('tglLhr');
        $user->telp2 = $this->request->getPost('telp2');
        $user->telp_wa = $this->request->getPost('telp2');
        $user->alamat = $this->request->getPost('alamat');
        $user->kelurahan = $this->request->getPost('kelurahan');
        $user->kecamatan = $this->request->getPost('kecamatan');
        $user->wilayah = $this->request->getPost('wilayah');
        $user->provinsi = $this->request->getPost('provinsi');
        $user->ijasah = $this->request->getPost('ijasah');
        $user->keilmuan = $this->request->getPost('keilmuan');
        $user->instansi = $this->request->getPost('instansi');
        $user->nama_instansi = $this->request->getPost('nama_instansi');
        $user->alamat_instansi = $this->request->getPost('alamat_instansi');
        $user->tglaktif = $this->request->getPost('tglaktif');
        $user->bidang = $this->request->getPost('bidang');
        $user->jabatan = $this->request->getPost('jabatan');
        $user->prov = $this->request->getPost('prov');
        $user->kodepos = $this->request->getPost('kodepos');
        $user->telp = $this->request->getPost('telp');
        $user->email_instansi = $this->request->getPost('email_instansi');
        $user->umur = $age->format("y");
        $users->save($user);

        return redirect()->to('admin')->with('message', 'Data Berhasil Diubah');
    }
}
