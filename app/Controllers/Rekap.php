<?php

namespace App\Controllers;

use \App\Models\PelatihanModel;
use \Myth\Auth\Models\UserModel;


class Rekap extends BaseController
{
    protected $pelatihanModel, $userModel;

    public function __construct()
    {
        $this->pelatihanModel = new PelatihanModel();
        $this->userModel = new UserModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page_rekap') ? $this->request->getVar('page_rekap') : 1;
        $data['currentPage'] = $currentPage;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $pelatihan = $this->pelatihanModel->orderBy("tglAwal", "Desc")->search($keyword);
        } else {
            $pelatihan = $this->pelatihanModel;
        }

        $data['title'] = 'Rekap Peserta';
        $data['pelatihan'] = $this->pelatihanModel->paginate(5, 'rekap');
        $data['pager'] = $this->pelatihanModel->pager;
        return view('admin/rekap/index', $data);
    }

    public function peserta($id)
    {
        $data['pelatihan'] = $this->pelatihanModel->find($id);
        $data['title'] = 'Rekap Peserta';
        $data['users'] = $this->userModel->getRekap($id);
        return view('admin/rekap/peserta', $data);
    }

    public function export($id)
    {
        //convert tanggal
        $pelatihan = $this->pelatihanModel->find($id);
        $tglAwal = $pelatihan['tglAwal'];
        $ConvtglAwal = date("d-m-Y", strtotime($tglAwal));
        $tglAkhir = $pelatihan['tglAkhir'];
        $ConvtglAkhir = date("d-m-Y", strtotime($tglAkhir));

        $data = [
            'pelatihan' => $pelatihan,
            'users' => $this->userModel->getRekap($id),
            'tglAwal' => $ConvtglAwal,
            'tglAkhir' => $ConvtglAkhir,
        ];

        return view('admin/rekap/excel', $data);
    }
}
