<?php

namespace App\Controllers;

use \Myth\Auth\Models\UserModel;
use \App\Models\PelatihanModel;
use \App\Models\BuktiModel;

class User extends BaseController
{
    protected $userModel, $pelatihanModel, $buktiModel;

    public function __construct()
    {
        $this->db      = \Config\Database::connect();
        $this->userModel = new UserModel();
        $this->pelatihanModel = new PelatihanModel();
        $this->buktiModel = new BuktiModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page_dashboard') ? $this->request->getVar('page_dashboard') : 1;
        $data['currentPage'] = $currentPage;

        $data['title'] = 'Dashboard';
        $data['pelatihan'] = $this->pelatihanModel->paginate(5, 'dashboard');
        $data['pager'] = $this->pelatihanModel->pager;
        return view('user/dashboard/index', $data);
    }

    public function profile()
    {
        $data['title'] = 'My Profile';
        return view('user/profile/index', $data);
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Form Mengubah Data',
            'user' => $this->userModel->find($id)
        ];

        return view('user/profile/edit', $data);
    }

    public function update($id)
    {
        $users = model(UserModel::class);
        $user = $this->userModel->find($id);
        //cek username
        $usernameLama = $this->userModel->find($id);
        if ($usernameLama->username == $this->request->getVar('username')) {
            $rule_username = 'required';
        } else {
            $rule_username = 'required|alpha_numeric_space|min_length[3]|max_length[30]|is_unique[users.username]';
        }


        $rules = [
            'username' => $rule_username,
            'nama'    => 'required',
            'jk'    => 'required',
            'tempatLhr'    => 'required',
            'tglLhr'    => 'required',
            'telp2'    => 'required',
            'alamat' => 'required',
            'kelurahan'    => 'required',
            'kecamatan'    => 'required',
            'wilayah'    => 'required',
            'provinsi'    => 'required',
            'ijasah'    => 'required',
            'keilmuan'    => 'required',
        ];

        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //input umur
        $tanggalSkrg = date("Y-m-d");
        $tglLhr = $this->request->getPost('tglLhr');
        $age = date_diff(date_create($tglLhr), date_create($tanggalSkrg));

        //update ke database
        $user->username = $this->request->getPost('username');
        $user->nama = $this->request->getPost('nama');
        $user->tempatLhr = $this->request->getPost('tempatLhr');
        $user->jk = $this->request->getPost('jk');
        $user->tglLhr = $this->request->getPost('tglLhr');
        $user->telp2 = $this->request->getPost('telp2');
        $user->telp_wa = $this->request->getPost('telp2');
        $user->alamat = $this->request->getPost('alamat');
        $user->kelurahan = $this->request->getPost('kelurahan');
        $user->kecamatan = $this->request->getPost('kecamatan');
        $user->wilayah = $this->request->getPost('wilayah');
        $user->provinsi = $this->request->getPost('provinsi');
        $user->keilmuan = $this->request->getPost('keilmuan');
        $user->ijasah = $this->request->getPost('ijasah');
        $user->umur = $age->format("y");
        $users->save($user);

        return redirect()->to('user')->with('message', 'Data Berhasil Diubah');
    }

    public function pelatihan()
    {
        $data['title'] = 'Pelatihan';
        $data['pelatihan'] = $this->pelatihanModel->findAll();

        return view('user/pelatihan/index', $data);
    }

    public function pilih()
    {
        $auth = service('authorization');

        $builder = $this->db->table('seleksi');
        $rules = [
            'pelatihan' => 'required',
            'nama_file' => 'uploaded[nama_file]|max_size[nama_file,4096]|mime_in[nama_file,application/pdf]',
        ];

        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //ambil Surat
        $fileSurat = $this->request->getFile('nama_file');
        //ambil nama Surat
        $namaSurat = $fileSurat->getRandomName();
        //pindahkan file ke Surat
        $fileSurat->move('surat_keterangan_aktif', $namaSurat);

        $data = [
            'user_id' => user()->id,
            'pelatihan_id' => $this->request->getVar('pelatihan'),
            'motivasi' => $this->request->getVar('motivasi'),
            'nama_file' => $namaSurat,
        ];

        $builder->insert($data);

        $auth->addPermissionToUser('pilih_pelatihan', user_id());

        return redirect()->to('user/pelatihan')->with('message', 'Berhasil memilih pelatihan');
    }

    public function bukti($id)
    {
        $data['pelatihan'] = $this->pelatihanModel->getPelatihanFromRekap($id);
        $data['title'] = 'Upload Bukti Pembayaran';

        return view('user/bukti/create', $data);
    }

    public function upload()
    {
        $auth = service('authorization');

        $rules = [
            'pelatihan_id' => 'required',
            'nominal' => 'required',
            'bukti_image' => 'uploaded[bukti_image]|max_size[bukti_image,2048]|is_image[bukti_image]|mime_in[bukti_image,image/jpg,image/jpeg,image/png]'
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //ambil gambar
        $fileBukti = $this->request->getFile('bukti_image');
        //ambil nama gambar
        $namaBukti = $fileBukti->getRandomName();
        //pindahkan file ke img
        $fileBukti->move('img/bukti-pembayaran', $namaBukti);

        //mengambil id user
        $userid = user_id();

        //mengambil id pelatihan
        $pelatihanid = $this->request->getVar('pelatihan_id');

        //save database
        $this->buktiModel->save([
            'user_id' => $userid,
            'pelatihan_id' => $pelatihanid,
            'nominal' => $this->request->getVar('nominal'),
            'bukti_image' => $namaBukti,
        ]);

        //menghapus permission upload bukti
        $auth->removePermissionFromUser('upload_bukti', $userid);

        //menambahkan permission sudah upload
        $auth->addPermissionToUser('sudah_upload_bukti', $userid);

        return redirect()->to('user')->with('message', 'Berhasil mengupload Bukti Pembayaran');
    }

    public function sertifikat($id)
    {
        $data['title'] = 'Daftar Sertifikat';

        //halaman
        $currentPage = $this->request->getVar('page_sertifikat') ? $this->request->getVar('page_sertifikat') : 1;
        $data['currentPage'] = $currentPage;

        //fungsi search data
        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $users = $this->userModel->searchSertifikatUser($keyword, $id);
        } else {
            $users = null;
        }

        $data['users'] = $this->userModel->getUserSertifikat($id, 2, 'sertifikat', $users);
        $data['pager'] = $this->userModel->pager;

        return view('user/sertifikat/index', $data);
    }

    public function download()
    {
        return $this->response->download('sertifikat/1650639399_33b8e773794bb1c1e8ee.pdf', null);
    }
}
