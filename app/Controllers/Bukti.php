<?php

namespace App\Controllers;

use App\Models\BuktiModel;
use \Myth\Auth\Models\UserModel;
use \App\Models\PelatihanModel;
use \App\Models\StatusModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;


class Bukti extends BaseController
{
    protected $userModel, $pelatihanModel, $statusModel, $buktiModel;

    public function __construct()
    {
        $this->db      = \Config\Database::connect();
        $this->userModel = new UserModel();
        $this->pelatihanModel = new PelatihanModel();
        $this->statusModel = new StatusModel();
        $this->buktiModel = new BuktiModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page_bukti') ? $this->request->getVar('page_bukti') : 1;
        $data['currentPage'] = $currentPage;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $pelatihan = $this->pelatihanModel->search($keyword);
        } else {
            $pelatihan = $this->pelatihanModel;
        }

        $data['title'] = 'Bukti Peserta';
        $data['pelatihan'] = $this->pelatihanModel->orderBy("tglAwal", "Desc")->paginate(5, 'bukti');
        $data['pager'] = $this->pelatihanModel->pager;
        return view('admin/bukti/index', $data);
    }

    public function peserta($id)
    {
        //fungsi search
        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $users = $this->userModel->searchBukti($id, $keyword);
        } else {
            $users = null;
        }

        $data['title'] = 'Daftar Peserta yang sudah membayar Pelatihan';
        $data['users'] = $this->userModel->getBukti($id, $users);
        $data['pelatihanid'] = $id;

        return view('admin/bukti/peserta', $data);
    }

    public function add($id)
    {
        $data['pelatihan'] = $this->pelatihanModel->find($id);
        $data['title'] = 'Tambah Bukti Pembayaran';
        $data['users'] = $this->userModel->getPesertaLolos($id);

        return view('admin/bukti/create', $data);
    }

    public function save()
    {
        $auth = service('authorization');

        $rules = [
            'user_id' => 'required',
            'bukti_image' => 'uploaded[bukti_image]|max_size[bukti_image,2048]|is_image[bukti_image]|mime_in[bukti_image,image/jpg,image/jpeg,image/png]'
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //ambil gambar
        $fileBukti = $this->request->getFile('bukti_image');
        //ambil nama gambar
        $namaBukti = $fileBukti->getRandomName();
        //pindahkan file ke img
        $fileBukti->move('img/bukti-pembayaran', $namaBukti);

        //mengambil id user
        $userid = $this->request->getVar('user_id');

        //mengambil id pelatihan
        $pelatihanid = $this->request->getVar('pelatihan_id');

        //save database
        $this->buktiModel->save([
            'user_id' => $userid,
            'pelatihan_id' => $pelatihanid,
            'nominal' => $this->request->getVar('nominal'),
            'bukti_image' => $namaBukti,
        ]);

        //menghapus permission upload bukti
        $auth->removePermissionFromUser('upload_bukti', $userid);

        //menambahkan permission sudah upload
        $auth->addPermissionToUser('sudah_upload_bukti', $userid);

        return redirect()->to('bukti/peserta/' . $pelatihanid)->with('message', 'Berhasil menambah peserta');
    }

    public function delete($id)
    {
        // cari gambar berdasarkan id
        $buktibayar = $this->buktiModel->find($id);
        //hapus gambar
        unlink('img/bukti-pembayaran/' . $buktibayar['bukti_image']);

        //menghapus data dari tabel bukti bayar
        $this->buktiModel->delete($id);

        return redirect()->to('bukti/peserta/' . $buktibayar['pelatihan_id'])->with('message', 'Berhasil menghapus data');
    }

    public function edit($id)
    {
        $bukti = $this->buktiModel->find($id);
        $data['pelatihan'] = $this->pelatihanModel->find($bukti['pelatihan_id']);
        $data['title'] = 'Edit Bukti Pembayaran';
        $data['users'] = $this->userModel->getPesertaLolos($bukti['pelatihan_id']);
        $data['bukti'] = $bukti;


        return view('admin/bukti/edit', $data);
    }

    public function update($id)
    {
        $auth = service('authorization');

        //validasi
        $rules = [
            'user_id' => 'required',
            'bukti_image' => 'max_size[bukti_image,2048]|is_image[bukti_image]|mime_in[bukti_image,image/jpg,image/jpeg,image/png]',
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //ambil gambar
        $fileBukti = $this->request->getFile('bukti_image');

        //cek gambar, apakah tetap sama
        if ($fileBukti->getError() == 4) {
            $namaBukti = $this->request->getVar('buktilama');
        } else {
            //generate random name
            $namaBukti = $fileBukti->getRandomName();
            //pindahkan gambar
            $fileBukti->move('img/bukti-pembayaran/', $namaBukti);
            //hapus gambar yang lama
            unlink('img/bukti-pembayaran/' . $this->request->getVar('buktilama'));
        }

        //mengambil id user
        $userid = $this->request->getVar('user_id');
        //mengambil id pelatihan
        $pelatihanid = $this->request->getVar('pelatihan_id');

        //save database
        $this->buktiModel->save([
            'id' => $id,
            'user_id' => $userid,
            'pelatihan_id' => $pelatihanid,
            'nominal' => $this->request->getVar('nominal'),
            'bukti_image' => $namaBukti,
        ]);

        //mengambil id user lama
        $iduserlama = $this->request->getVar('iduserlama');
        //menghapus permission upload bukti
        $auth->removePermissionFromUser('sudah_upload_bukti', $iduserlama);
        //menambahkan permission sudah upload
        $auth->addPermissionToUser('upload_bukti', $iduserlama);

        //menghapus permission upload bukti
        $auth->removePermissionFromUser('upload_bukti', $userid);
        //menambahkan permission sudah upload
        $auth->addPermissionToUser('sudah_upload_bukti', $userid);

        return redirect()->to('bukti/peserta/' . $pelatihanid)->with('message', 'Berhasil mengubah data');
    }

    public function download($id)
    {
        $data = $this->buktiModel->find($id);
        return $this->response->download('img/bukti-pembayaran/' . $data['bukti_image'], null);
    }

    public function export($id)
    {

        $users = $this->userModel->getExportBukti($id);
        $pelatihan = $this->pelatihanModel->find($id);

        //conv tanggal
        $tglAwal = $pelatihan['tglAwal'];
        $ConvtglAwal = date("d-m-Y", strtotime($tglAwal));
        $tglAkhir = $pelatihan['tglAkhir'];
        $ConvtglAkhir = date("d-m-Y", strtotime($tglAkhir));

        //table head style
        $tableHead = [
            'font' => [
                'color' => [
                    'rgb' => 'FFFFFF'
                ],
                'bold' => true,
                'size' => 11,
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'F96262'
                ],
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' =>  Border::BORDER_THIN,
                ]
            ]
        ];


        //make a new spreadsheet object
        $spreadsheet = new Spreadsheet();
        //get current active sheet
        $sheet = $spreadsheet->getActiveSheet();

        //set default font
        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(10);

        //Heading tabel
        $spreadsheet->getActiveSheet()
            ->getCell('A1')
            ->setValue('Total Data Terpilih Semua dari ' . count($users) . ' dari total ' . $pelatihan['jumlah_peserta'] . ' Data Registrasi ' . $pelatihan['nama'] . ', Tanggal ' . $ConvtglAwal . ' s/d ' . $ConvtglAkhir);

        //merge Heading
        $spreadsheet->getActiveSheet()->mergeCells("A1:U1");

        //set font style
        $spreadsheet->getActiveSheet()->getStyle('A1')->getFont()->setSize('16');

        //set bold style
        $spreadsheet->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        //set column width
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
        $spreadsheet->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);

        $spreadsheet->getActiveSheet()
            ->setCellValue('A3', 'No')
            ->setCellValue('B3', 'Provinsi')
            ->setCellValue('C3', 'Instansi')
            ->setCellValue('D3', 'Nama')
            ->setCellValue('E3', 'L/P')
            ->setCellValue('F3', 'ID')
            ->setCellValue('G3', 'Tempat Lahir')
            ->setCellValue('H3', 'Tanggal Lahir')
            ->setCellValue('I3', 'Alamat')
            ->setCellValue('J3', 'No HP')
            ->setCellValue('K3', 'No WA')
            ->setCellValue('L3', 'Email')
            ->setCellValue('M3', 'Pendidikan')
            ->setCellValue('N3', 'Jurusan')
            ->setCellValue('O3', 'Jabatan')
            ->setCellValue('P3', 'Bidang')
            ->setCellValue('Q3', 'Aktif di Instansi')
            ->setCellValue('R3', 'Alamat Instansi')
            ->setCellValue('S3', 'No Telp Instansi')
            ->setCellValue('T3', 'Email Instansi')
            ->setCellValue('U3', 'Bukti Transfer');
        $column = 4;

        //SET FONT STYLE and background color
        $spreadsheet->getActiveSheet()->getStyle('A3:U3')->applyFromArray($tableHead);


        foreach ($users as $user) {
            $i = 1;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $column, $i)
                ->setCellValue('B' . $column, $user->prov)
                ->setCellValue('C' . $column, $user->nama_instansi)
                ->setCellValue('D' . $column, $user->namauser)
                ->setCellValue('E' . $column, (($user->jk == 0) ? "Laki-Laki" : "Perempuan"))
                ->setCellValue('F' . $column, $user->userid)
                ->setCellValue('G' . $column, $user->tempatLhr)
                ->setCellValue('H' . $column, $user->tglLhr)
                ->setCellValue('I' . $column, $user->alamat . ', ' . $user->kelurahan . ', ' . $user->kecamatan . ', ' . $user->wilayah . ', ' . $user->provinsi)
                ->setCellValue('J' . $column, $user->telp2)
                ->setCellValue('K' . $column, $user->telp2)
                ->setCellValue('L' . $column, $user->email)
                ->setCellValue('M' . $column, $user->ijasah)
                ->setCellValue('N' . $column, $user->keilmuan)
                ->setCellValue('O' . $column, $user->jabatan)
                ->setCellValue('P' . $column, $user->bidang)
                ->setCellValue('Q' . $column, $user->tglaktif)
                ->setCellValue('R' . $column, $user->alamat_instansi)
                ->setCellValue('S' . $column, $user->telp)
                ->setCellValue('T' . $column, $user->email_instansi)
                ->setCellValue('U' . $column, $user->namauser);
            $column++;
        }

        //set the header first, so the result will be treated as an xlsx file.
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        //make it an attachment so we can define filename
        header('Content-Disposition: attachment;filename="rekap_kegiatan_diklat.xlsx"');

        //create object
        $writer = new Xlsx($spreadsheet);
        //save into php output
        $writer->save('php://output');
    }
}
