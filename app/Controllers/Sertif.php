<?php

namespace App\Controllers;

use \App\Models\PelatihanModel;
use \Myth\Auth\Models\UserModel;
use \App\Models\SertifikatModel;


class Sertif extends BaseController
{
    protected $pelatihanModel, $userModel, $sertifikatModel;

    public function __construct()
    {
        $this->pelatihanModel = new PelatihanModel();
        $this->userModel = new UserModel();
        $this->sertifikatModel = new SertifikatModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page_sertifikat') ? $this->request->getVar('page_sertifikat') : 1;
        $data['currentPage'] = $currentPage;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $pelatihan = $this->pelatihanModel->orderBy("tglAwal", "Desc")->search($keyword);
        } else {
            $pelatihan = $this->pelatihanModel;
        }

        $data['title'] = 'Sertifikat Peserta';
        $data['pelatihan'] = $this->pelatihanModel->paginate(5, 'sertifikat');
        $data['pager'] = $this->pelatihanModel->pager;
        return view('admin/sertifikat/index', $data);
    }

    public function peserta($id)
    {
        //fungsi search
        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $users = $this->userModel->searchSertifikat($id, $keyword);
        } else {
            $users = null;
        }
        $pelatihan = $this->pelatihanModel->find($id);

        $data['title'] = 'Sertifikat Peserta';
        $data['pelatihan'] = $pelatihan;
        $data['users'] = $this->userModel->getSertifikat($id, $users);
        return view('admin/sertifikat/peserta', $data);
    }

    public function add($id)
    {
        $data['pelatihan'] = $this->pelatihanModel->find($id);
        $data['title'] = 'Tambah Sertifikat';
        $data['users'] = $this->userModel->getUserFromBukti($id);

        return view('admin/sertifikat/create', $data);
    }

    public function save()
    {
        $auth = service('authorization');

        $rules = [
            'user_id' => 'required',
            'nama_file' => 'uploaded[nama_file]|max_size[nama_file,4096]|mime_in[nama_file,application/pdf]'
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //ambil sertifikat
        $fileSertifikat = $this->request->getFile('nama_file');
        //ambil nama sertifikat
        $namaSertifikat = $fileSertifikat->getRandomName();
        //pindahkan file ke sertifikat
        $fileSertifikat->move('sertifikat', $namaSertifikat);

        //mengambil id pelatihan yang dipilih oleh peserta
        $pelatihanId = $this->request->getVar('pelatihan_id');
        //mengambil id peserta
        $userId = $this->request->getVar('user_id');


        //save database
        $this->sertifikatModel->save([
            'user_id' => $userId,
            'pelatihan_id' => $pelatihanId,
            'nama_file' => $namaSertifikat,
        ]);

        //mengahapus permission sudah upload
        $auth->removePermissionFromUser('sudah_upload_bukti', $userId);

        return redirect()->to('sertif/peserta/' . $pelatihanId)->with('message', 'Berhasil menambahkan data');
    }

    public function delete($id)
    {
        // cari file berdasarkan id
        $sertifikat = $this->sertifikatModel->find($id);
        //hapus gambar
        unlink('sertifikat/' . $sertifikat['nama_file']);

        //menghapus data dari tabel bukti bayar
        $this->sertifikatModel->delete($id);

        return redirect()->to('sertif/peserta/' . $sertifikat['pelatihan_id'])->with('message', 'Berhasil menghapus data');
    }

    public function edit($id)
    {
        $sertifikat = $this->sertifikatModel->find($id);
        $data['sertifikat'] = $sertifikat;
        $data['pelatihan'] = $this->pelatihanModel->find($sertifikat['pelatihan_id']);
        $data['title'] = 'Edit Sertifikat';
        $data['users'] = $this->userModel->getUserFromBukti($sertifikat['pelatihan_id']);

        return view('admin/sertifikat/edit', $data);
    }

    public function update($id)
    {
        $auth = service('authorization');

        $rules = [
            'user_id' => 'required',
            'nama_file' => 'max_size[nama_file,4096]|mime_in[nama_file,application/pdf]'
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        //ambil file
        $fileSertif = $this->request->getFile('nama_file');

        //cek file, apakah tetap sama
        if ($fileSertif->getError() == 4) {
            $namaSertif = $this->request->getVar('sertifikatlama');
        } else {
            //generate random name
            $namaSertif = $fileSertif->getRandomName();
            //pindahkan gambar
            $fileSertif->move('sertifikat/', $namaSertif);
            //hapus gambar yang lama
            unlink('sertifikat/' . $this->request->getVar('sertifikatlama'));
        }

        //mengambil id user yang lama  
        $iduserlama = $this->request->getVar('iduserlama');

        //menambahkan role upload bukti
        $auth->addPermissionToUser('sudah_upload_bukti', $iduserlama);

        //mengambil id pelatihan yang dipilih oleh peserta
        $pelatihanId = $this->request->getVar('pelatihan_id');
        //mengambil id peserta
        $userId = $this->request->getVar('user_id');

        $this->sertifikatModel->save([
            'id' => $id,
            'user_id' => $userId,
            'nama_file' => $namaSertif
        ]);

        return redirect()->to('sertif/peserta/' . $pelatihanId)->with('message', 'Berhasil mengubah data');
    }
}
