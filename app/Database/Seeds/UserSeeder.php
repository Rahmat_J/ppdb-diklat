<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Myth\Auth\Test\Fakers\UserFaker;

class UserSeeder extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 25; $i++) {
            helper('test');

            $user = fake(UserFaker::class);
        }
        return $user;
    }
}
