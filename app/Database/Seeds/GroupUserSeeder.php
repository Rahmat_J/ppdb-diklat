<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class GroupUserSeeder extends Seeder
{
    public function run()
    {
        for ($i = 4; $i <= 28; $i++) {
            $data = [
                'group_id' => 2,
                'user_id' => $i,
            ];
            $this->db->table('auth_groups_users')->insert($data);
        }
    }
}
