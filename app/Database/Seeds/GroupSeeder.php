<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class GroupSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'name' => 'admin',
            ],
            [
                'name' => 'user'
            ]
        ];

        // Using Query Builder
        $this->db->table('auth_groups')->insertBatch($data);
    }
}
