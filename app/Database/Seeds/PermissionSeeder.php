<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class PermissionSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'name' => 'upload_bukti',
                'description' => 'Filter untuk peserta yang sudah lolos seleksi dan bisa mengupload bukti pembayaran'
            ],
            [
                'name' => 'sudah_upload_bukti',
                'description' => 'Filter untuk peserta yang sudah mengupload bukti bayar'
            ],
            [
                'name' => 'pilih_pelatihan',
                'description' => 'Filter untuk peserta yang sudah memilih pelatihan'
            ],
        ];

        // Using Query Builder
        $this->db->table('auth_permissions')->insertBatch($data);
    }
}
