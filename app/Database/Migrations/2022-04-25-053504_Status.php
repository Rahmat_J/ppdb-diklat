<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Status extends Migration
{
    public function up()
    {
        /* 
        Tabel Status
        */
        $fields = [
            'id' => ['type' => 'INT', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
            'user_id'  => ['type' => 'int', 'constraint' => 11, 'unsigned' => true],
            'pelatihan_id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true],
            'status' => ['type' => 'tinyint', 'constraint' => 1, 'unsigned' => true, 'null' => true],
            'created_at' => ['type' => 'datetime', 'null' => true],
            'updated_at' => ['type' => 'datetime', 'null' => true],
        ];

        $this->forge->addField($fields);
        $this->forge->addKey(['id', 'user_id', 'pelatihan_id']);
        $this->forge->addForeignKey('user_id', 'users', 'id', '', 'CASCADE');
        $this->forge->addForeignKey('pelatihan_id', 'pelatihan', 'id', '', 'CASCADE');
        $this->forge->createTable('status');
    }

    public function down()
    {
        // drop constraints first to prevent errors
        if ($this->db->DBDriver != 'SQLite3') // @phpstan-ignore-line
        {
            $this->forge->dropForeignKey('status', 'status_user_id_foreign');
            $this->forge->dropForeignKey('status', 'status_pelatihan_id_foreign');
        }
        $this->forge->dropTable('status');
    }
}
