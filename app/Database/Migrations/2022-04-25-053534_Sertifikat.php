<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Sertifikat extends Migration
{
    public function up()
    {
        /*
            Tabel Sertifikat
        */
        $this->forge->addField([
            'id'          => ['type'           => 'INT', 'constraint'     => 11, 'unsigned'       => true, 'auto_increment' => true,],
            'user_id'  => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'default' => 0],
            'pelatihan_id'  => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'default' => 0],
            'nama_file' => ['type'       => 'VARCHAR', 'constraint' => '255', 'null' => true,],
            'created_at' => ['type' => 'datetime', 'null' => true],
            'updated_at' => ['type' => 'datetime', 'null' => true],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addKey(['user_id', 'pelatihan_id']);
        $this->forge->addForeignKey('user_id', 'users', 'id', '', 'CASCADE');
        $this->forge->addForeignKey('pelatihan_id', 'pelatihan', 'id', '', 'CASCADE');
        $this->forge->createTable('sertifikat');
    }

    public function down()
    {
        if ($this->db->DBDriver != 'SQLite3') // @phpstan-ignore-line
        {
            $this->forge->dropForeignKey('sertifikat', 'sertifikat_user_id_foreign');
            $this->forge->dropForeignKey('sertifikat', 'sertifikat_pelatihan_id_foreign');
        }

        $this->forge->dropTable('sertifikat');
    }
}
