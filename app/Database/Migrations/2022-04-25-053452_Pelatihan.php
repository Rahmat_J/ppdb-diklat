<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pelatihan extends Migration
{
    public function up()
    {
        /*
            Tabel Pelatihan
        */
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'nama'       => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null' => true,
            ],
            'kategori'       => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null' => true,
            ],
            'tglAwal' => [
                'type' => 'date',
                'null' => true
            ],
            'tglAkhir' => [
                'type' => 'date',
                'null' => true
            ],
            'jumlah_peserta' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'tempat' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null' => true,
            ],
            'penyelenggara' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null' => true,
            ],
            'skp_id' => [
                'type'       => 'CHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'biaya' => [
                'type'       => 'INT',
                'constraint' => '20',
                'null' => true,
            ],
            'created_at' => [
                'type' => 'datetime',
                'null' => true
            ],
            'updated_at' => [
                'type' => 'datetime',
                'null' => true
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('pelatihan');
    }

    public function down()
    {
        $this->forge->dropTable('pelatihan');
    }
}
