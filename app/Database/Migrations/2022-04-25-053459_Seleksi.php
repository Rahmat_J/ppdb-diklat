<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Seleksi extends Migration
{
    public function up()
    {
        /* 
        Tabel Seleksi
        */
        $fields = [
            'id' => ['type' => 'INT', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
            'user_id'  => ['type' => 'int', 'constraint' => 11, 'unsigned' => true],
            'pelatihan_id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true],
            'nama_file' => ['type' => 'VARCHAR', 'constraint' => '255', 'null' => true],
            'motivasi' => ['type' => 'VARCHAR', 'constraint' => '255', 'null' => true],
            'created_at' => ['type' => 'datetime', 'null' => true],
            'updated_at' => ['type' => 'datetime', 'null' => true],
        ];

        $this->forge->addField($fields);
        $this->forge->addKey(['id', 'user_id', 'pelatihan_id']);
        $this->forge->addForeignKey('user_id', 'users', 'id', '', 'CASCADE');
        $this->forge->addForeignKey('pelatihan_id', 'pelatihan', 'id', '', 'CASCADE');
        $this->forge->createTable('seleksi');
    }

    public function down()
    {
        // drop constraints first to prevent errors
        if ($this->db->DBDriver != 'SQLite3') // @phpstan-ignore-line
        {
            $this->forge->dropForeignKey('seleksi', 'seleksi_user_id_foreign');
            $this->forge->dropForeignKey('seleksi', 'seleksi_pelatihan_id_foreign');
        }
        $this->forge->dropTable('seleksi');
    }
}
