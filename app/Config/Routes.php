<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'User::index');
$routes->group('', ['filter' => 'role:admin'], function ($routes) {
    //restriction buat user untuk mengakses controller Pelatihan
    $routes->get('pelatihan', 'Pelatihan::index');
    $routes->get('pelatihan/index', 'Pelatihan::index');
    $routes->get('pelatihan/add', 'Pelatihan::add');
    $routes->get('pelatihan/save', 'Pelatihan::save');
    $routes->get('pelatihan/save/(:any)', 'Pelatihan::save/$1');
    $routes->get('pelatihan/delete', 'Pelatihan::delete');
    $routes->get('pelatihan/delete/(:any)', 'Pelatihan::delete/$1');
    $routes->get('pelatihan/edit', 'Pelatihan::edit');
    $routes->get('pelatihan/edit/(:any)', 'Pelatihan::edit/$1');
    $routes->get('pelatihan/update/', 'Pelatihan::update');
    $routes->get('pelatihan/update/(:any)', 'Pelatihan::update/$1');

    //restriction buat user untuk mengakses controller admin
    $routes->get('admin', 'Admin::index');
    $routes->get('admin/index', 'Admin::index');
    $routes->get('admin/detail', 'Admin::detail');
    $routes->get('admin/detail/(:any)', 'Admin::detail/$1');
    $routes->get('admin/add', 'Admin::add');
    $routes->get('admin/save', 'Admin::save');
    $routes->get('admin/save/(:any)', 'Admin::save/$1');
    $routes->get('admin/delete', 'Admin::delete');
    $routes->get('admin/delete/(:any)', 'Admin::delete/$1');
    $routes->get('admin/edit', 'Admin::edit');
    $routes->get('admin/edit/(:any)', 'Admin::edit/$1');
    $routes->get('admin/update/', 'Admin::update');
    $routes->get('admin/update/(:any)', 'Admin::update/$1');

    //restriction buat user untuk mengakses controller Seleksi
    $routes->get('seleski', 'Seleski::index');
    $routes->get('seleski/peserta', 'Seleski::peserta');
    $routes->get('seleski/peserta/(:any)', 'Seleski::peserta/$1');
    $routes->get('seleski/add', 'Seleski::add');
    $routes->get('seleski/save', 'Seleski::save');
    $routes->get('seleski/tolak', 'Seleski::tolak');
    $routes->get('seleski/tolak/(:any)', 'Seleski::tolak/$1');
    $routes->get('seleski/terima', 'Seleski::terima');
    $routes->get('seleski/terima/(:any)', 'Seleski::terima/$1');

    //restriction buat user untuk mengakses controller Rekap
    $routes->get('rekap', 'Rekap::index');
    $routes->get('rekap/peserta', 'Rekap::peserta');
    $routes->get('rekap/peserta/(:any)', 'Rekap::peserta/$1');
    $routes->get('rekap/export', 'Rekap::export');
    $routes->get('rekap/export/(:any)', 'Rekap::export/$1');

    //restriction buat user untuk mengakses controller Bukti
    $routes->get('bukti', 'Bukti::index');
    $routes->get('bukti/index', 'Bukti::index');
    $routes->get('bukti/peserta', 'Bukti::peserta');
    $routes->get('bukti/peserta/(:any)', 'Bukti::peserta/$1');
    $routes->get('bukti/add', 'Bukti::add');
    $routes->get('bukti/add/(:any)', 'Bukti::add/$1');
    $routes->get('bukti/save', 'Bukti::save');
    $routes->get('bukti/save/(:any)', 'Bukti::save/$1');
    $routes->get('bukti/delete', 'Bukti::delete');
    $routes->get('bukti/delete/(:any)', 'Bukti::delete/$1');
    $routes->get('bukti/edit', 'Bukti::edit');
    $routes->get('bukti/edit/(:any)', 'Bukti::edit/$1');
    $routes->get('bukti/update/', 'Bukti::update');
    $routes->get('bukti/update/(:any)', 'BuktiBayar::update/$1');
    $routes->get('bukti/export', 'Bukti::export');
    $routes->get('bukti/export/(:any)', 'Bukti::export/$1');

    //restriction buat user untuk mengakses controller Pelatihan
    $routes->get('sertif', 'Sertif::index');
    $routes->get('sertif/index', 'Sertif::index');
    $routes->get('sertif/peserta', 'Sertif::peserta');
    $routes->get('sertif/peserta/(:any)', 'Sertif::peserta/$1');
    $routes->get('sertif/add', 'Sertif::add');
    $routes->get('sertif/add/(:any)', 'Sertif::add/$1');
    $routes->get('sertif/save', 'Sertif::save');
    $routes->get('sertif/save/(:any)', 'Sertif::save/$1');
    $routes->get('sertif/delete', 'Sertif::delete');
    $routes->get('sertif/delete/(:any)', 'Sertif::delete/$1');
    $routes->get('sertif/edit', 'Sertif::edit');
    $routes->get('sertif/edit/(:any)', 'Sertif::edit/$1');
    $routes->get('sertif/update/', 'Sertif::update');
    $routes->get('sertif/update/(:any)', 'Sertif::update/$1');
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
