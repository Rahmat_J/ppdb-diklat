<?php

namespace App\Models;

use CodeIgniter\Model;

class BuktiModel extends Model
{
    protected $table      = 'bukti';
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $allowedFields = ['nominal', 'user_id', 'pelatihan_id', 'bukti_image'];
}
