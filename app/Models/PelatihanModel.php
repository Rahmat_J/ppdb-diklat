<?php

namespace App\Models;

use CodeIgniter\Model;

class PelatihanModel extends Model
{
    protected $table      = 'pelatihan';
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama', 'kategori', 'tglAwal', 'tglAkhir', 'tempat', 'penyelenggara', 'skp_id', 'biaya', 'jumlah_peserta'];

    /** 
     * Mengambil 1 buah data dari tabel pelatihan
     * 
     * @param int  $id
     * 
     * @return array
     */
    public function getPelatihan($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }

    /** 
     * Mencari data yang sama dengan inputan dari tabel pelatihan
     * 
     * @param string  $keyword
     * 
     * @return array
     */
    public function search($keyword)
    {
        // $builder = $this->table('orang');
        // $builder->like('nama', $keyword);
        // return $builder;

        return $this->table('pelatihan')->like('id', $keyword)->orLike('nama', $keyword);
    }

    /** 
     * Mencari data pelatihan yang telah lulus seleksi
     * 
     * @param string  $keyword
     * 
     * @return array
     */
    public function getPelatihanFromRekap($id)
    {
        $builder = $this->db->table('pelatihan');
        $builder->select('pelatihan.id as pelatihanid, nama, biaya');
        $builder->join('status', 'status.pelatihan_id = pelatihan.id');
        $builder->where('status.user_id', $id);
        $builder->where('status.status', 1);
        $query = $builder->get()->getResultArray();
        $pelatihan = array_column($query, 'nama', 'pelatihanid');

        return $pelatihan;
    }


    /** 
     * Mengambil semua data  pelatihan berurutan tanggal
     * 
     * @param string  $nb
     * @param string  $tablename
     * 
     * @return array
     */
    public function allPelatihan($nb, $tablename, $data = null)
    {
        // if ($data == null) {
        //     $builder = $this->db->table('pelatihan');
        //     $builder->select('*');
        //     $builder->orderBy('tglAwal');

        //     return $builder->paginate($nb, $tablename);
        // } else {
        //     return $data->paginate($nb, $tablename);
        // }

        // if ($data == null) {

        //     $builder = $this->db->table('pelatihan');
        //     $builder->select('*');
        //     $builder->orderBy('tglAwal');

        //     return $builder->paginate($nb, $tablename);
        // } else {
        //     return $data->paginate($nb, $tablename);
        // }
    }
}
