<?php

namespace App\Models;

use CodeIgniter\Model;

class SeleksiModel extends Model
{
    protected $table      = 'seleksi';
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $allowedFields = ['user_id', 'pelatihan_id', 'motivasi', 'nama_file'];

    /** 
     * Mengambil 1 buah data dari tabel seleksi
     * 
     * @param int  $id
     * 
     * @return array
     */
    public function getSeleksi($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }
}
