<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>
<section class="content-header">
    <div class="container">
        <div class="row justify-content-center mb-2">
            <div class="col-sm-8 text-center">
                <h1>Tambah Bukti Pembayaran</h1>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    <?php if (has_permission('pilih_pelatihan')) : ?>
                        <div class="card-header">
                        </div>
                        <div class="container">
                            <h2 class="text-center bukti">Data Anda sedang diverifikasi</h2>
                        </div>
                    <?php elseif (has_permission('upload_bukti')) : ?>
                        <div class="card-header">
                            <h3 class="card-title">Bukti Pembayaran</h3>
                        </div>
                        <form action="<?= base_url("/user/upload"); ?>" method="POST" enctype="multipart/form-data">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Pelatihan yang diikuti</label>
                                    <select name="pelatihan_id" class="form-control <?php if (session('errors.pelatihan_id')) : ?>is-invalid<?php endif ?>">
                                        <option selected disabled>--Pilih Salah Satu--</option>
                                        <?php foreach ($pelatihan as $id => $nama) : ?>
                                            <option value="<?= $id; ?>" <?= old('pelatihan_id') == $id ? "selected" : ""; ?>><?= $nama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Nominal</label>
                                    <input class="form-control <?php if (session('errors.nominal')) : ?>is-invalid<?php endif ?>" name="nominal" placeholder="Tanpa titik dan koma" value="<?= old('nominal'); ?>">
                                </div>
                                <div class="form-group">
                                    <label>Foto Bukti Transfer</label>
                                    <div class="input-group row">
                                        <div class="col-sm-3">
                                            <img src="<?= base_url("/img/bukti-pembayaran/default.jpg"); ?>" class="img-thumbnail img-preview">
                                        </div>
                                        <div class="custom-file col-8">
                                            <input type="file" class="custom-file-input <?php if (session('errors.bukti_image')) : ?>is-invalid<?php endif ?>" id="foto" name="bukti_image" value="<?= old('bukti_image'); ?>" onchange="previewImg()">
                                            <label for="foto" class="custom-file-label">Foto berupa .png/.jpg/.jpeg</label>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">Submit</button>
                            </div>
                        </form>
                    <?php elseif (has_permission('sudah_upload_bukti')) : ?>
                        <div class="card-header">
                        </div>
                        <div class="container">
                            <h2 class="text-center bukti">Terimakasih telah melakukan pembayaran silahkan mengikuti pelatihan pada tanggal yang tertera</h2>
                        </div>
                    <?php else : ?>
                        <div class="card-header">
                        </div>
                        <div class="container">
                            <h2 class="text-center bukti">Anda belum memilih pelatihan</h2>
                        </div>
                    <?php endif; ?>

                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>


</section>

<?= $this->endSection(); ?>