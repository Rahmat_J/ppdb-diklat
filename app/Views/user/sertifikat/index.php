<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Sertifikat Pelatihan</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-body">
            <?php if ($users == null) : ?>
                <div class="container">
                    <h2 class="text-center bukti">Anda belum mempunyai sertifikat</h2>
                </div>

            <?php else : ?>
                <form action="" method="POST">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Masukkan Nama Pelatihan" name="keyword">
                        <button class="btn btn-outline-secondary" type="submit" name="submit">Cari</button>
                    </div>
                </form>
                <table class="table table-responsive table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">No</th>
                            <th>Nama Peserta</th>
                            <th>Pelatihan yang diambil</th>
                            <th>Tanggal Pelaksanaan</th>
                            <th>File Sertifikat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1 + (2 * ($currentPage - 1)); ?>
                        <?php foreach ($users as $user) : ?>
                            <tr>
                                <th scope="row"><?= $i++; ?></th>
                                <td><?= $user->namauser; ?></td>
                                <td><?= $user->namapelatihan; ?></td>
                                <td><?= $user->tglAwal; ?> s/d <?= $user->tglAkhir; ?></td>
                                <td class="text-center"><iframe src="<?= base_url('sertifikat/' . $user->nama_file); ?>" width="420" height="400"></iframe></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <?= $pager->links('sertifikat', 'pagination') ?>
        </div>
    </div>

</section>
<!-- /.content -->

<?= $this->endSection(); ?>