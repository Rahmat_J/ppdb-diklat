<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>
<section class="content-header">
    <div class="container">
        <div class="row justify-content-center mb-2">
            <div class="col-sm-6 text-center">
                <h1>Pilih Pelatihan</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card card-primary">
                <?= view('Myth\Auth\Views\_message_block') ?>
                <div class="card-header">
                    <h3 class="card-title">Pelatihan</h3>
                </div>
                <form action="<?= base_url('/user/pilih'); ?>" method="POST" enctype="multipart/form-data">
                    <?= csrf_field(); ?>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Pelatihan yang ingin diikuti</label>
                            <select class="form-control <?php if (session('errors.pelatihan')) : ?>is-invalid<?php endif ?>" name="pelatihan">
                                <option selected disabled>--Pilih Salah Satu--</option>
                                <?php foreach ($pelatihan as $p) : ?>
                                    <option value="<?= $p['id']; ?>"><?= $p['nama']; ?> | <?= $p['tglAwal']; ?> s/d <?= $p['tglAkhir']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Motivasi</label>
                            <input class="form-control" name="motivasi" value="<?= old('motivasi') ?>">
                        </div>
                        <label>Surat Keterangan Aktif di Instansi</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="nama_file" class="custom-file-input <?php if (session('errors.nama_file')) : ?>is-invalid<?php endif ?>" id="file" value="<?= old('nama_file'); ?>" onchange="previewDocs()">
                                <label for="" class="custom-file-label">Upload File .pdf</label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                    </div>
                </form>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>


</section>

<?= $this->endSection(); ?>