<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <?= view('Myth\Auth\Views\_message_block') ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header text-center">
                        <h4>Jadwal Pelatihan</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">No</th>
                                            <th>Nama</th>
                                            <th>Tanggal Pelaksanaan</th>
                                            <th>Tanggal Selesai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1 + (5 * ($currentPage - 1)); ?>
                                        <?php foreach ($pelatihan as $p) : ?>
                                            <tr>
                                                <td><?= $i++; ?></td>
                                                <td><?= $p['nama']; ?></td>
                                                <td><?= $p['tglAwal']; ?></td>
                                                <td><?= $p['tglAkhir']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer clearfix">
                        <?= $pager->links('dashboard', 'pagination') ?>
                    </div>
                </div>
            </div>
            <!-- </div> -->
            <!-- <div class="row"> -->
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header text-center">
                        <h4>Panduan Penggunaan Aplikasi</h4>
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <div class="dashboard">
                                    <div class="row">
                                        <div class="col">
                                            <iframe src="<?= base_url('file/panduan_penggunaan_aplikasi.pdf'); ?>"></iframe>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col text-center">
                                            <a href="<?= base_url('user/download'); ?>" class="btn btn-primary">Download File</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<?= $this->endSection(); ?>