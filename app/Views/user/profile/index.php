<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>My Profile</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <?= view('Myth\Auth\Views\_message_block') ?>
        <div class="row">
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="<?= base_url() ?>/img/user-image/<?= user()->user_image; ?>">
                        </div>
                        <h3 class="profile-username text-center"><?= user()->nama; ?></h3>
                        <br>
                        <!-- <p class="text-muted text-center">Software Engineer</p> -->

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Username</b>
                                <p class="float-right"><?= user()->username; ?></p>
                            </li>
                            <li class="list-group-item">
                                <b>Email</b>
                                <p class="float-right"><?= user()->email; ?></p>
                            </li>
                            <!-- <li class="list-group-item">
                                <b>Password</b> <a href="" class="float-right">Reset Password</a>
                            </li> -->
                            <br>
                            <a href="<?= base_url('user/edit/' . user()->id); ?>" class="btn btn-warning"><i class="fa-regular fa-pen-to-square"></i> Edit Profile</a>
                        </ul>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>
            <div class="col-md-9">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Data Pribadi</h3>
                    </div>

                    <div class="card-body">
                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>

                        <p class="text-muted"><?= user()->alamat; ?> <?= user()->kelurahan; ?>, <?= user()->kecamatan; ?>, <?= user()->wilayah; ?>, <?= user()->provinsi; ?>.</p>

                        <hr>


                        <strong><i class="fas fa-phone"></i> Nomor Handphone</strong>

                        <p class="text-muted"><?= user()->telp2; ?> </p>

                        <hr>

                        <strong><i class="fa-solid fa-calendar-day"></i> Tempat, Tanggal Lahir</strong>

                        <p class="text-muted"><?= user()->tempatLhr; ?>, <?= user()->tglLhr; ?> </p>

                        <hr>

                        <strong><i class="fa-solid fa-user-graduate"></i> Lulusan</strong>

                        <p class="text-muted"><?= user()->ijasah; ?> <?= user()->keilmuan; ?></p>

                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Data Instansi</h3>
                    </div>

                    <div class="card-body">
                        <strong><i class="fa-solid fa-building"></i> <?= user()->instansi; ?></strong>

                        <p class="text-muted"><?= user()->nama_instansi; ?></p>

                        <hr>

                        <strong><i class="fa-solid fa-location-arrow"></i>Alamat</strong>

                        <p class="text-muted"><?= user()->alamat_instansi; ?>, <?= user()->prov; ?>, <?= user()->kodepos; ?> </p>

                        <hr>

                        <strong><i class="fas fa-phone"></i> Nomor Telepon</strong>

                        <p class="text-muted"><?= user()->telp; ?> </p>

                        <hr>

                        <strong><i class="fa-solid fa-envelope"></i> Email</strong>

                        <p class="text-muted"><?= user()->email_instansi; ?> </p>

                        <hr>


                        <strong><i class="fa-solid fa-user-tie"></i> Status</strong>

                        <p class="text-muted"><?= user()->jabatan; ?> <?= user()->bidang; ?></p>

                        <hr>

                        <strong><i class="fa-solid fa-calendar-days"></i> Aktif Sejak</strong>

                        <p class="text-muted"><?= user()->tglaktif; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<?= $this->endSection(); ?>