<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Form Mengubah Peserta</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="card card-outline card-primary">
        <div class="card-body">
            <p class="login-box-msg">Masukkan data Yang Sesuai!!</p>
            <?= view('Myth\Auth\Views\_message_block') ?>
            <form action="<?= base_url('/user/update/' . user()->id) ?>" method="post">
            <div class="row">
                    <!-- left side -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group col-7">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control <?php if (session('errors.nama')) : ?>is-invalid<?php endif ?>" name="nama" placeholder="Nama Lengkap" value="<?= (old('nama')) ? old('nama') : $user->nama; ?>">
                            </div>
                            <div class="form-group col-5">
                                <label>Username</label>
                                <input type="text" class="form-control <?php if (session('errors.username')) : ?>is-invalid<?php endif ?>" name="username" placeholder="Username" value="<?= (old('username')) ? old('username') : $user->username; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email address</label>
                            <input type="email" class="form-control" name="email" placeholder="<?= lang('Auth.email') ?>" value="<?= $user->email; ?>" disabled>
                            <small id="emailHelp" class="form-text text-muted">Email tidak bisa diganti</small>
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin :</label>
                            <select class="form-control col-5 <?php if (session('errors.jk')) : ?>is-invalid<?php endif ?>" name="jk">
                                <option value="" disabled>--Pilih Salah Satu--</option>
                                <option value="0" <?= $user->jk == "0" ? "selected" : ''; ?>>Laki-Laki</option>
                                <option value="1" <?= $user->jk == "1" ? "selected" : ''; ?>>Perempuan</option>
                            </select>
                        </div>
                        <div class="row ">
                            <div class="form-group col-8 ">
                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control <?php if (session('errors.tempatLhr')) : ?>is-invalid<?php endif ?>" name="tempatLhr" placeholder="Tempat Lahir" value="<?= (old('tempatLhr')) ? old('tempatLhr') : $user->tempatLhr; ?>">
                            </div>
                            <div class="form-group col-4">
                                <label>Tanggal Lahir</label>
                                <input type="text" class="form-control datepicker <?php if (session('errors.tglLhr')) : ?>is-invalid<?php endif ?>" name="tglLhr" placeholder="yyyy-mm-dd" value="<?= (old('tglLhr')) ? old('tglLhr') : $user->tglLhr; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-4">
                                <label>Jenjang</label>
                                <select class="form-control <?php if (session('errors.provinsi')) : ?>is-invalid<?php endif ?>" name="ijasah">
                                    <option value="" disabled>--Pilih Salah Satu--</option>
                                    <option value="SMA" <?= $user->ijasah == "SMA" ? "selected" : ''; ?>>SMA</option>
                                    <option value="D3" <?= $user->ijasah == "D3" ? "selected" : ''; ?>>D3</option>
                                    <option value="Profesi" <?= $user->ijasah == "Profesi" ? "selected" : ''; ?>>Profesi</option>
                                    <option value="S1" <?= $user->ijasah == "S1" ? "selected" : ''; ?>>S1</option>
                                    <option value="S2" <?= $user->ijasah == "S2" ? "selected" : ''; ?>>S2</option>
                                </select>
                            </div>
                            <div class="form-group col-8">
                                <label>Jurusan</label>
                                <input type="text" name="keilmuan" class="form-control <?php if (session('errors.keilmuan')) : ?>is-invalid<?php endif ?>" placeholder="Keilmuan" value="<?= (old('keilmuan')) ? old('keilmuan') : $user->keilmuan; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nomor HP</label>
                            <input type="text" class="form-control <?php if (session('errors.telp2')) : ?>is-invalid<?php endif ?>" name="telp2" placeholder="Nomor Handphone" value="<?= (old('telp2')) ? old('telp2') : $user->telp2; ?>">
                            <input type="hidden" name="telp_wa" value="">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="alamat" class="form-control <?php if (session('errors.alamat')) : ?>is-invalid<?php endif ?>" placeholder="Jalan, RT/RW" value="<?= (old('alamat')) ? old('alamat') : $user->alamat; ?>">
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <input type="text" name="kelurahan" class="form-control <?php if (session('errors.kelurahan')) : ?>is-invalid<?php endif ?>" placeholder="Kelurahan" value="<?= (old('kelurahan')) ? old('kelurahan') : $user->kelurahan; ?>">
                            </div>
                            <div class="form-group col-6">
                                <input type="text" name="kecamatan" class="form-control <?php if (session('errors.kecamatan')) : ?>is-invalid<?php endif ?>" placeholder="Kecamatan" value="<?= (old('kecamatan')) ? old('kecamatan') : $user->kecamatan; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <input type="text" name="wilayah" class="form-control <?php if (session('errors.wilayah')) : ?>is-invalid<?php endif ?>" placeholder="Kabupaten/Kota" value="<?= (old('wilayah')) ? old('wilayah') : $user->wilayah; ?>">
                            </div>
                            <div class="form-group col-6">
                                <input type="text" name="provinsi" class="form-control <?php if (session('errors.provinsi')) : ?>is-invalid<?php endif ?>" placeholder="Provinsi" value="<?= (old('provinsi')) ? old('provinsi') : $user->provinsi; ?>">
                            </div>
                        </div>
                    </div>

                    <!-- right side -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Data Instansi</label>
                            <div class="row">
                                <div class="form-group col-4">
                                    <select class="form-control <?php if (session('errors.instansi')) : ?>is-invalid<?php endif ?>" name="instansi">
                                        <option value="" disabled>--Pilih Salah Satu--</option>
                                        <option value="UDD" <?= $user->instansi == "UDD" ? "selected" : ''; ?>>UDD PMI</option>
                                        <option value="RS" <?= $user->instansi == "RS" ? "selected" : ''; ?>>RS</option>
                                    </select>
                                </div>
                                <div class="form-group col-8">
                                    <input type="text" class="form-control <?php if (session('errors.nama_instansi')) : ?>is-invalid<?php endif ?>" placeholder="Nama Instansi" name="nama_instansi" value="<?= (old('nama_instansi')) ? old('nama_instansi') : $user->nama_instansi; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control <?php if (session('errors.alamat_instansi')) : ?>is-invalid<?php endif ?>" placeholder="Alamat Instansi" name="alamat_instansi" value="<?= (old('alamat_instansi')) ? old('alamat_instansi') : $user->alamat_instansi; ?>">
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <input type="text" class="form-control <?php if (session('errors.prov')) : ?>is-invalid<?php endif ?>" placeholder="Kab/Kota" name="prov" value="<?= (old('prov')) ? old('prov') : $user->prov; ?>">
                                </div>
                                <div class="form-group col-6">
                                    <input type="text" class="form-control <?php if (session('errors.kodepos')) : ?>is-invalid<?php endif ?>" placeholder="Kode Pos" name="kodepos" value="<?= (old('kodepos')) ? old('kodepos') : $user->kodepos; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control <?php if (session('errors.telp')) : ?>is-invalid<?php endif ?>" placeholder="Nomor Telepon Instansi" name="telp" value="<?= (old('telp')) ? old('telp') : $user->telp; ?>">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control <?php if (session('errors.email_instansi')) : ?>is-invalid<?php endif ?>" name="email_instansi" placeholder="Email Instansi" value="<?= (old('email_instansi')) ? old('email_instansi') : $user->email_instansi; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Bekerja di Bagian</label>
                            <input type="text" class="form-control <?php if (session('errors.bidang')) : ?>is-invalid<?php endif ?>" placeholder="Nama Bidang" name="bidang" value="<?= (old('bidang')) ? old('bidang') : $user->bidang; ?>">
                        </div>
                        <div class="form-group">
                            <label>Jabatan</label>
                            <input type="text" class="form-control <?php if (session('errors.jabatan')) : ?>is-invalid<?php endif ?>" placeholder="Jabatan" name="jabatan" value="<?= (old('jabatan')) ? old('jabatan') : $user->jabatan; ?>">
                        </div>
                        <div class="form-group">
                            <label>Aktif di Instansi Sejak:</label>
                            <input type="text" class="form-control datepicker <?php if (session('errors.tglaktif')) : ?>is-invalid<?php endif ?>" placeholder=" yyyy-mm-dd" name="tglaktif" value="<?= (old('tglaktif')) ? old('tglaktif') : $user->tglaktif; ?>">
                        </div>
                    </div>
                </div>
                <hr>
                <!-- /.col -->
                <div class="middle">
                    <button type="submit" class="btn btn-primary btn-block">Update</button>
                </div>
                <!-- /.col -->
            </form>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
</section>
<?= $this->endSection(); ?>