<?= $this->extend('auth/template/index'); ?>
<?= $this->section('content'); ?>

<div class="register-box">
  <div class="register-logo">
    <a href=""><b>UDDP-</b>PMI</a>
  </div>
  <div class="card card-outline card-primary">
    <div class="card-body">
      <p class="login-box-msg">Masukkan data Yang Sesuai!!</p>
      <form action="<?= route_to('register') ?>" method="post">
        <?= csrf_field(); ?>
        <div class="row">
          <!-- left side -->
          <div class="col-md-6">
            <div class="row">
              <div class="form-group col-7">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control <?php if (session('errors.nama')) : ?>is-invalid<?php endif ?>" name="nama" placeholder="Nama Lengkap" value="<?= old('nama') ?>">
              </div>
              <div class="form-group col-5">
                <label>Username</label>
                <input type="text" class="form-control <?php if (session('errors.username')) : ?>is-invalid<?php endif ?>" name="username" placeholder="Username" value="<?= old('username') ?>">
              </div>
            </div>
            <div class="form-group">
              <label>Email address</label>
              <input type="email" class="form-control <?php if (session('errors.email')) : ?>is-invalid<?php endif ?>" name="email" placeholder="<?= lang('Auth.email') ?>" value="<?= old('email') ?>">
              <small id="emailHelp" class="form-text text-muted"><?= lang('Auth.weNeverShare') ?></small>
            </div>
            <div class="form-group">
              <label>Jenis Kelamin :</label>
              <select class="form-control col-5 <?php if (session('errors.jk')) : ?>is-invalid<?php endif ?>" name="jk">
                <option value="" selected disabled>--Pilih Salah Satu--</option>
                <option value="0" <?= old('jk') == "0" ? "selected" : ''; ?>>Laki-Laki</option>
                <option value="1" <?= old('jk') == "1" ? "selected" : ''; ?>>Perempuan</option>
              </select>
            </div>
            <div class="row ">
              <div class="form-group col-8 ">
                <label>Tempat Lahir</label>
                <input type="text" class="form-control <?php if (session('errors.tempatLhr')) : ?>is-invalid<?php endif ?>" name="tempatLhr" placeholder="Tempat Lahir" value="<?= old('tempatLhr') ?>">
              </div>
              <div class="form-group col-4">
                <label>Tanggal Lahir</label>
                <input type="text" class="form-control datepicker <?php if (session('errors.tglLhr')) : ?>is-invalid<?php endif ?>" name="tglLhr" placeholder="yyyy-mm-dd" value="<?= old('tglLhr') ?>">
              </div>
            </div>
            <div class="form-group">
              <label>Nomor HP</label>
              <input type="text" class="form-control <?php if (session('errors.telp2')) : ?>is-invalid<?php endif ?>" name="telp2" placeholder="Nomor Handphone" value="<?= old('telp2') ?>">
              <input type="hidden" name="telp_wa" value="">
            </div>
            <div class="form-group">
              <label>Alamat</label>
              <input type="text" class="form-control <?php if (session('errors.alamat')) : ?>is-invalid<?php endif ?>" placeholder="Jalan, RT/RW" name="alamat" value="<?= old('alamat') ?>">
            </div>
            <div class="row">
              <div class="form-group col-6">
                <input type="text" class="form-control <?php if (session('errors.kelurahan')) : ?>is-invalid<?php endif ?>" placeholder="Kelurahan" name="kelurahan" value="<?= old('kelurahan') ?>">
              </div>
              <div class="form-group col-6">
                <input type="text" class="form-control <?php if (session('errors.kecamatan')) : ?>is-invalid<?php endif ?>" placeholder="Kecamatan" name="kecamatan" value="<?= old('kecamatan') ?>">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-6">
                <input type="text" class="form-control <?php if (session('errors.wilayah')) : ?>is-invalid<?php endif ?>" placeholder="Kabupaten/Kota" name="wilayah" value="<?= old('wilayah') ?>">
              </div>
              <div class="form-group col-6">
                <input type="text" class="form-control <?php if (session('errors.provinsi')) : ?>is-invalid<?php endif ?>" placeholder="Provinsi" name="provinsi" value="<?= old('provinsi') ?>">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-4">
                <label>Jenjang</label>
                <select class="form-control <?php if (session('errors.keilmuan')) : ?>is-invalid<?php endif ?>" name="ijasah">
                  <option value="" selected disabled>--Pilih Salah Satu--</option>
                  <option value="SMA" <?= old('ijasah') == "SMA" ? "selected" : ''; ?>>SMA</option>
                  <option value="D3" <?= old('ijasah') == "D3" ? "selected" : ''; ?>>D3</option>
                  <option value="Profesi" <?= old('ijasah') == "Profesi" ? "selected" : ''; ?>>Profesi</option>
                  <option value="S1" <?= old('ijasah') == "S1" ? "selected" : ''; ?>>S1</option>
                  <option value="S2" <?= old('ijasah') == "S2" ? "selected" : ''; ?>>S2</option>
                </select>
              </div>
              <div class="form-group col-8">
                <label>Jurusan</label>
                <input type="text" class="form-control <?php if (session('errors.keilmuan')) : ?>is-invalid<?php endif ?>" placeholder="jurusan" name="keilmuan" value="<?= old('keilmuan') ?>">
              </div>
            </div>
          </div>
          <!-- right side -->
          <div class="col-md-6">
            <div class="form-group">
              <label>Data Instansi</label>
              <div class="row">
                <div class="form-group col-4">
                  <select class="form-control <?php if (session('errors.instansi')) : ?>is-invalid<?php endif ?>" name="instansi">
                    <option value="" selected disabled>--Pilih Salah Satu--</option>
                    <option value="UDD" <?= old('instansi') == "UDD" ? "selected" : ''; ?>>UDD PMI</option>
                    <option value="RS" <?= old('instansi') == "RS" ? "selected" : ''; ?>>RS</option>
                  </select>
                </div>
                <div class="form-group col-8">
                  <input type="text" class="form-control <?php if (session('errors.nama_instansi')) : ?>is-invalid<?php endif ?>" placeholder="Nama Instansi" name="nama_instansi" value="<?= old('nama_instansi') ?>">
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control <?php if (session('errors.alamat_instansi')) : ?>is-invalid<?php endif ?>" placeholder="Alamat Instansi" name="alamat_instansi" value="<?= old('alamat_instansi') ?>">
              </div>
              <div class="row">
                <div class="form-group col-6">
                  <input type="text" class="form-control <?php if (session('errors.prov')) : ?>is-invalid<?php endif ?>" placeholder="Kab/Kota" name="prov" value="<?= old('prov') ?>">
                </div>
                <div class="form-group col-6">
                  <input type="text" class="form-control <?php if (session('errors.kodepos')) : ?>is-invalid<?php endif ?>" placeholder="Kode Pos" name="kodepos" value="<?= old('kodepos') ?>">
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control <?php if (session('errors.telp')) : ?>is-invalid<?php endif ?>" placeholder="Nomor Telepon Instansi" name="telp" value="<?= old('telp') ?>">
              </div>
              <div class="form-group">
                <input type="email" class="form-control <?php if (session('errors.email_instansi')) : ?>is-invalid<?php endif ?>" name="email_instansi" placeholder="Email Instansi" value="<?= old('email_instansi') ?>">
              </div>
            </div>
            <div class="form-group">
              <label>Bekerja di Bagian</label>
              <input type="text" class="form-control <?php if (session('errors.bidang')) : ?>is-invalid<?php endif ?>" placeholder="Nama Bidang" name="bidang" value="<?= old('bidang') ?>">
            </div>
            <div class="form-group">
              <label>Jabatan</label>
              <input type="text" class="form-control <?php if (session('errors.jabatan')) : ?>is-invalid<?php endif ?>" placeholder="Jabatan" name="jabatan" value="<?= old('jabatan') ?>">
            </div>
            <div class="form-group">
              <label>Aktif di Instansi Sejak:</label>
              <input type="text" class="form-control datepicker <?php if (session('errors.tglaktif')) : ?>is-invalid<?php endif ?>" placeholder=" yyyy-mm-dd" name="tglaktif" value="<?= old('tglaktif') ?>">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control <?php if (session('errors.password')) : ?>is-invalid<?php endif ?>" placeholder="Password" autocomplete="off">
            </div>
            <div class="form-group">
              <label>Konfirmasi Password</label>
              <input type="password" name="pass_confirm" class="form-control <?php if (session('errors.pass_confirm')) : ?>is-invalid<?php endif ?>" placeholder="Ulangi Password" autocomplete="off">
            </div>
          </div>
        </div>
        <hr>
        <!-- /.col -->
        <div class="middle">
          <button type="submit" class="btn btn-primary btn-block"><?= lang('Auth.register') ?></button>
        </div>
        <!-- /.col -->
      </form>
      <br>
      <a href="<?= route_to('login') ?>">Sudah Punya Akun?</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->
<?= $this->endSection(); ?>