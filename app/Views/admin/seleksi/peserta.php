<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Seleksi Peserta <?= $pelatihan['nama']; ?></h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <?= view('Myth\Auth\Views\_message_block') ?>
            <div class="card-tools">
                <a class="btn btn-primary" href="<?= base_url('seleksi/add'); ?>">+ Tambah Seleksi Peserta</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th style="width: 10px">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Tanggal Lahir</th>
                                <th scope="col">JK</th>
                                <th scope="col">Ijasah</th>
                                <th scope="col">Jurusan</th>
                                <th scope="col">Instansi</th>
                                <th scope="col">Surat Keterangan</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 ?>
                            <?php foreach ($users as $user) : ?>
                                <tr>
                                    <th scope="row"><?= $i++; ?></th>
                                    <td><?= $user->namauser; ?></td>
                                    <td><?= $user->tglLhr; ?></td>
                                    <td><?= ($user->jk == 0) ? 'L' : 'P' ?></td>
                                    <td><?= $user->ijasah; ?></td>
                                    <td><?= $user->keilmuan; ?></td>
                                    <td><?= $user->nama_instansi; ?></td>
                                    <td class="text-center"><iframe src="<?= base_url('surat_keterangan_aktif/' . $user->nama_file); ?>" width="420" height="400"></iframe></td>
                                    <td>
                                        <a href="<?= base_url('seleksi/tolak/' . $user->seleksiid); ?>" onclick="return confirm('Apakah anda yakin menolak peserta?')" class="btn btn-warning">Tolak</a>
                                        <a href="<?= base_url('seleksi/terima/' . $user->seleksiid); ?>" onclick="return confirm('Apakah anda yakin menerima peserta?')" class="btn btn-success">Terima</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>


<?= $this->endSection(); ?>