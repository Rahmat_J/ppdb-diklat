<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>
<section class="content-header">
    <div class="container">
        <div class="row justify-content-center mb-2">
            <div class="col-sm-6 text-center">
                <h1>Tambah Peserta yang Diseleksi</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card card-primary">
                <?= view('Myth\Auth\Views\_message_block') ?>
                <div class="card-header">
                </div>
                <form action="<?= base_url('/seleksi/save'); ?>" method="POST" enctype="multipart/form-data">
                    <?= csrf_field(); ?>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Peserta</label>
                            <select class="form-control <?php if (session('errors.peserta')) : ?>is-invalid<?php endif ?>" name="peserta">
                                <option selected disabled>--Pilih Salah Satu--</option>
                                <?php foreach ($users as $user) : ?>
                                    <option value="<?= $user->id; ?>" <?= old('peserta') == $user->id ? "selected" : ""; ?>><?= $user->nama; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pelatihan yang ingin diikuti</label>
                            <select class="form-control <?php if (session('errors.pelatihan')) : ?>is-invalid<?php endif ?>" name="pelatihan">
                                <option selected disabled>--Pilih Salah Satu--</option>
                                <?php foreach ($pelatihan as $p) : ?>
                                    <option value="<?= $p['id']; ?>" <?= old('pelatihan') == $p['id'] ? "selected" : ""; ?>><?= $p['nama']; ?> | <?= $p['tglAwal']; ?> s/d <?= $p['tglAkhir']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>File Surat</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="nama_file" class="custom-file-input <?php if (session('errors.nama_file')) : ?>is-invalid<?php endif ?>" id="file" value="<?= old('nama_file'); ?>" onchange="previewDocs()">
                                    <label for="" class="custom-file-label">Upload File .pdf</label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                    </div>
                </form>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>


</section>

<?= $this->endSection(); ?>