<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Seleksi Peserta</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <?= view('Myth\Auth\Views\_message_block') ?>
                        <div class="row">
                            <div class="col-7">
                                <form action="" method="POST">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Masukkan Nama Pelatihan" name="keyword">
                                        <button class="btn btn-outline-secondary" type="submit" name="submit">Cari</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col">
                                <a class="btn btn-primary float-right" href="<?= base_url('seleksi/add'); ?>">+ Tambah Seleksi Peserta</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-auto">
                                <table class="table table-responsive table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">No</th>
                                            <th>Nama</th>
                                            <th>Tanggal Pelaksanaan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1 + (5 * ($currentPage - 1)); ?>
                                        <?php foreach ($pelatihan as $p) : ?>
                                            <tr>
                                                <th scope="row"><?= $i++; ?></th>
                                                <td><?= $p['nama']; ?></td>
                                                <td><?= $p['tglAwal']; ?> s/d <?= $p['tglAkhir']; ?></td>
                                                <td>
                                                    <a href="<?= base_url('/seleksi/peserta/' . $p['id']); ?>" class="btn btn-info">Lihat Peserta</a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        <?= $pager->links('seleksi', 'pagination') ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>


<?= $this->endSection(); ?>