<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Peserta</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <?= view('Myth\Auth\Views\_message_block') ?>
            <div class="row">
                <div class="col-6">
                    <form action="" method="POST">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Masukkan id atau nama" name="keyword">
                            <button class="btn btn-outline-secondary" type="submit" name="submit">Cari</button>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <a class="btn btn-primary float-right" href="<?= base_url('admin/add'); ?>">+ Tambah Peserta</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <table class="table table-responsive table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 10px">No</th>
                                <th>Nama</th>
                                <th style="width: 10px">ID</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Nomor HP</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 + (5 * ($currentPage - 1)); ?>
                            <?php foreach ($users as $user) : ?>
                                <tr>
                                    <td><?= $i++; ?></td>
                                    <td><?= $user->nama; ?></td>
                                    <th scope="row"><?= $user->id; ?></th>
                                    <td><?= $user->username; ?></td>
                                    <td><?= $user->email; ?></td>
                                    <td><?= $user->telp2; ?></td>
                                    <td>
                                        <a href="<?= base_url('admin/detail/' . $user->id); ?>" class="btn btn-secondary">Detail</a>
                                        <a href="<?= base_url('admin/edit/' . $user->id); ?>" class="btn btn-warning">Edit</a>
                                        <a href="<?= base_url('admin/delete/' . $user->id); ?>" class="btn btn-danger" onclick="return confirm('Apakah kamu yakin menghapus data?')">Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <?= $pager->links('users', 'pagination') ?>
        </div>
    </div>

</section>
<!-- /.content -->

<?= $this->endSection(); ?>