<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">

            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">

            <div class="card-tools">
                <a class="btn btn-success" href="<?= base_url('rekap/export/' . $pelatihan['id']); ?>">Export</a>
            </div>
            <div class="col-sm-6">
                <h3>Rekap Seleksi Peserta <?= $pelatihan['nama']; ?> </h3>
            </div>

        </div>
        <div class="col">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr style="background-color: crimson; color:white">
                            <th scope="col">No</th>
                            <th scope="col">Provinsi</th>
                            <th scope="col">Instansi</th>
                            <th scope="col">Nama</th>
                            <th scope="col">L/P</th>
                            <th scope="col">ID</th>
                            <th scope="col">Tempat Lahir</th>
                            <th scope="col">Tanggal Lahir</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">No HP</th>
                            <th scope="col">No WA</th>
                            <th scope="col">Email</th>
                            <th scope="col">Pendidikan</th>
                            <th scope="col">Jurusan</th>
                            <th scope="col">Jabatan</th>
                            <th scope="col">Bidang</th>
                            <th scope="col">Aktif Di Instansi</th>
                            <th scope="col">Alamat Instansi</th>
                            <th scope="col">No Telp Instansi</th>
                            <th scope="col">Email Instansi</th>
                            <th scope="col">Biaya</th>
                            <th scope="col">Status</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($users as $user) : ?>
                            <tr style="vertical-align: middle;">
                                <th><?= $i++; ?></th>
                                <td><?= $user->prov; ?></td>
                                <td><?= $user->nama_instansi; ?></td>
                                <td><?= $user->namauser; ?></td>
                                <td><?= ($user->jk) == 0 ? "Laki-Laki" : "Perempuan"; ?></td>
                                <td><?= $user->userid; ?></td>
                                <td><?= $user->tempatLhr; ?></td>
                                <td><?= $user->tglLhr; ?></td>
                                <td><?= $user->alamat; ?> <?= $user->kelurahan; ?>, <?= $user->kecamatan; ?>, <?= $user->wilayah; ?>, <?= $user->provinsi; ?>.</td>
                                <td><?= $user->telp2; ?></td>
                                <td><?= $user->telp2; ?></td>
                                <td><?= $user->email; ?></td>
                                <td><?= $user->ijasah; ?></td>
                                <td><?= $user->keilmuan; ?></td>
                                <td><?= $user->jabatan; ?></td>
                                <td><?= $user->bidang; ?></td>
                                <td><?= $user->tglaktif; ?></td>
                                <td><?= $user->alamat_instansi; ?></td>
                                <td><?= $user->telp; ?></td>
                                <td><?= $user->email_instansi; ?></td>
                                <td><?= $user->biaya; ?></td>
                                <td><?= ($user->lolos) == 1 ? "Lolos" : "Tidak Lolos"; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>


<?= $this->endSection(); ?>