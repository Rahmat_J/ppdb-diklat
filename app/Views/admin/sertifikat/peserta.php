<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Sertifikat yang mengikuti <?= $pelatihan['nama']; ?></h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <?= view('Myth\Auth\Views\_message_block') ?>
            <div class="row">
                <div class="col-6">
                    <form action="" method="POST">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Masukkan nama" name="keyword">
                            <button class="btn btn-outline-secondary" type="submit" name="submit">Cari</button>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <a class="btn btn-primary float-right" href="<?= base_url('sertif/add/' . $pelatihan['id']); ?>">+ Tambah Sertifikat</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">No</th>
                        <th>Nama Peserta</th>
                        <th>File Sertifikat</th>
                        <th style="width: fit-content;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <th scope="row"><?= $i++; ?></th>
                            <td><?= $user->namauser; ?></td>
                            <td><iframe src="<?= base_url('/sertifikat/' . $user->nama_file); ?>" width="500" height="300"></iframe></td>
                            <td>
                                <a href="<?= base_url('sertif/edit/' . $user->sertifikatid); ?>" class="btn btn-warning">Edit</a>
                                <a href="<?= base_url('sertif/delete/' . $user->sertifikatid); ?>" class="btn btn-danger" onclick="return confirm('Apakah kamu yakin menghapus data?')">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
</section>
<!-- /.content -->

<?= $this->endSection(); ?>