<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>
<section class="content-header">
    <div class="container">
        <div class="row justify-content-center mb-2">
            <div class="col-sm-8 text-center">
                <h1>Tambah Sertifikat</h1>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Bukti Pembayaran</h3>
                    </div>
                    <?= view('Myth\Auth\Views\_message_block') ?>

                    <form action="<?= base_url("/sertif/save"); ?>" method="POST" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Pelatihan yang diikuti</label>
                                <select name="pelatihan_id" class="form-control" readonly="true">
                                    <option value="<?= $pelatihan['id']; ?>"><?= $pelatihan['nama']; ?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Peserta</label>
                                <select class="form-control <?php if (session('errors.user_id')) : ?>is-invalid<?php endif ?>" name="user_id">
                                    <option selected disabled>--Pilih Salah Satu--</option>
                                    <?php foreach ($users as $id => $nama) : ?>
                                        <option value="<?= $id; ?>" <?= old('user_id') == $id ? "selected" : ""; ?>><?= $nama; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>File Sertifikat</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input <?php if (session('errors.nama_file')) : ?>is-invalid<?php endif ?>" id="file" name="nama_file" value="<?= old('nama_file'); ?>" onchange="previewDocs()">
                                        <label for="sertifikat" class="custom-file-label">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <a href="<?= base_url('sertif/peserta/' . $pelatihan['id']); ?>" class="btn btn-warning">Back</a>
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>
                    </form>

                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>


</section>

<?= $this->endSection(); ?>