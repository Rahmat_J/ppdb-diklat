<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>
<section class="content-header">
    <div class="container-fluid">
        <div class="row justify-content-center mb-2">
            <div class="col-sm-6 text-center">
                <h1>Edit Pelatihan</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card card-outline card-primary">
                <div class="card-header">
                </div>
                <form action="<?= base_url('/pelatihan/update/' . $pelatihan['id']) ?>" method="POST">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" class="form-control <?php if (session('errors.nama')) : ?>is-invalid<?php endif ?>" value="<?= (old('nama')) ? old('nama') : $pelatihan['nama']; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Kategori</label>
                                    <select class="form-control col-5 <?php if (session('errors.kategori')) : ?>is-invalid<?php endif ?>" name="kategori">
                                        <option value="" selected disabled>--Pilih Salah Satu--</option>
                                        <option value="NON TEKNIS" <?= $pelatihan['kategori'] == "NON TEKNIS" ? "selected" : ""; ?>>NON TEKNIS</option>
                                        <option value="TEKNIS" <?= $pelatihan['kategori'] == "TEKNIS" ? "selected" : ""; ?>>TEKNIS</option>
                                        <option value="ONLINE" <?= $pelatihan['kategori'] == "ONLINE" ? "selected" : ""; ?>>ONLINE</option>
                                    </select>
                                </div>
                                <div class="row ">
                                    <div class="form-group col-6">
                                        <label>Rencana Tanggal Dilaksanakan</label>
                                        <input type="text" class="form-control datepicker <?php if (session('errors.tglAwal')) : ?>is-invalid<?php endif ?>" name="tglAwal" placeholder="yyyy-mm-dd" value="<?= (old('tglAwal')) ? old('tglAwal') : $pelatihan['tglAwal']; ?>">
                                    </div>
                                    <div class="form-group col-6">
                                        <label>Rencana Tanggal Selesai</label>
                                        <input type="text" class="form-control datepicker <?php if (session('errors.tglAkhir')) : ?>is-invalid<?php endif ?>" name="tglAkhir" placeholder="yyyy-mm-dd" value="<?= (old('tglAkhir')) ? old('tglAkhir') : $pelatihan['tglAkhir']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Jumlah Peserta</label>
                                    <input type="text" name="jumlah_peserta" class="form-control <?php if (session('errors.jumlah_peserta')) : ?>is-invalid<?php endif ?>" value="<?= (old('jumlah_peserta')) ? old('jumlah_peserta') : $pelatihan['jumlah_peserta']; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tempat Pelaksanaan</label>
                                    <input type="text" name="tempat" class="form-control <?php if (session('errors.tempat')) : ?>is-invalid<?php endif ?>" value="<?= (old('tempat')) ? old('tempat') : $pelatihan['tempat']; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Penyelenggara</label>
                                    <input type="text" name="penyelenggara" class="form-control <?php if (session('errors.penyelenggara')) : ?>is-invalid<?php endif ?>" value="<?= (old('penyelenggara')) ? old('penyelenggara') : $pelatihan['penyelenggara']; ?>">
                                </div>
                                <div class="form-group">
                                    <label>ID SKP PELATIHAN</label>
                                    <input type="text" name="skp_id" class="form-control <?php if (session('errors.skp_id')) : ?>is-invalid<?php endif ?>" value="<?= (old('skp_id')) ? old('skp_id') : $pelatihan['skp_id']; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Biaya</label>
                                    <input type="text" name="biaya" placeholder="Tanpa Titik dan Koma" class="form-control <?php if (session('errors.biaya')) : ?>is-invalid<?php endif ?>" value="<?= (old('biaya')) ? old('biaya') : $pelatihan['biaya']; ?>">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                    </div>
            </div>
            </form>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    </div>


</section>

<?= $this->endSection(); ?>