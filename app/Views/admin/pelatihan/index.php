<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Pelatihan</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <?= view('Myth\Auth\Views\_message_block') ?>
            <div class="row">
                <div class="col-6">
                    <form action="" method="POST">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Masukkan Nama Pelatihan" name="keyword">
                            <button class="btn btn-outline-secondary" type="submit" name="submit">Cari</button>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <a class="btn btn-primary float-right" href="<?= base_url('pelatihan/add'); ?>">+ Tambah Pelatihan</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <table class="table table-responsive table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 10px">No</th>
                                <th>Nama</th>
                                <th>Kategori</th>
                                <th>Peserta</th>
                                <th>Tanggal Pelaksanaan</th>
                                <th>Tanggal Selesai</th>
                                <th>ID SKP Pelatihan</th>
                                <th>Tempat</th>
                                <th>Penyelenggara</th>
                                <th>Biaya</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 + (5 * ($currentPage - 1)); ?>
                            <?php foreach ($pelatihan as $p) : ?>
                                <tr>
                                    <th scope="row"><?= $i++; ?></th>
                                    <td><?= $p['nama']; ?></td>
                                    <td><?= $p['kategori']; ?></td>
                                    <td><?= $p['jumlah_peserta']; ?></td>
                                    <td><?= $p['tglAwal']; ?></td>
                                    <td><?= $p['tglAkhir']; ?></td>
                                    <td><?= $p['skp_id']; ?></td>
                                    <td><?= $p['tempat']; ?></td>
                                    <td><?= $p['penyelenggara']; ?></td>
                                    <td><?= $p['biaya']; ?></td>
                                    <td>
                                        <a href="<?= base_url('/pelatihan/edit/' . $p['id']); ?>" class="btn btn-warning">Edit</a>
                                        <a href="<?= base_url('/pelatihan/delete/' . $p['id']); ?>" class="btn btn-danger" onclick="return confirm('Apakah kamu yakin menghapus data?')">Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <?= $pager->links('pelatihan', 'pagination') ?>
        </div>
    </div>

</section>
<!-- /.content -->

<?= $this->endSection(); ?>