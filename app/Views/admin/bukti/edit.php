<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>
<section class="content-header">
    <div class="container">
        <div class="row justify-content-center mb-2">
            <div class="col-sm-8 text-center">
                <h1>Edit Bukti Pembayaran</h1>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Bukti Pembayaran</h3>
                    </div>
                    <?= view('Myth\Auth\Views\_message_block') ?>

                    <form action="<?= base_url("/bukti/update/" . $pelatihan['id']); ?>" method="POST" enctype="multipart/form-data">
                        <div class="card-body">
                            <input type="hidden" name="iduserlama" value="<?= $bukti['user_id']; ?>">
                            <input type="hidden" name="buktilama" value="<?= $bukti['bukti_image']; ?>">
                            <div class="form-group">
                                <label>Pelatihan yang diikuti</label>
                                <select name="pelatihan_id" class="form-control" readonly="true">
                                    <option value="<?= $pelatihan['id']; ?>"><?= $pelatihan['nama']; ?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Peserta</label>
                                <select class="form-control <?php if (session('errors.user_id')) : ?>is-invalid<?php endif ?>" name="user_id">
                                    <option selected disabled>--Pilih Salah Satu--</option>
                                    <?php foreach ($users as $id => $nama) : ?>
                                        <option value="<?= $id; ?>" <?= ($id == $bukti['user_id']) ? "selected" : ""; ?>><?= $nama; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nominal</label>
                                <input class="form-control" name="nominal" value="<?= $pelatihan['biaya'] ?>" readonly="true">
                            </div>
                            <div class="form-group">
                                <label>Foto Bukti Transfer</label>
                                <div class="input-group row">
                                    <div class="col-sm-3">
                                        <img src="<?= base_url('/img/bukti-pembayaran/' . $bukti['bukti_image']); ?>" class="img-thumbnail img-preview">
                                    </div>
                                    <div class="custom-file col-8">
                                        <input type="file" class="custom-file-input <?php if (session('errors.bukti_image')) : ?>is-invalid<?php endif ?>" id="foto" name="bukti_image" value="<?= old('bukti_image'); ?>" onchange="previewImg()">
                                        <label for="foto" class="custom-file-label"><?= $bukti['bukti_image']; ?></label>
                                    </div>
                                </div>
                            </div>
                            <a href="<?= base_url('bukti/peserta/' . $pelatihan['id']); ?>" class="btn btn-warning">Back</a>
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>
                    </form>

                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>


</section>

<?= $this->endSection(); ?>