<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename= Rekap Bukti Pembayaran Pelatihan.xls");
?>
<html>

<body>
    <h2>Total Data Terpilih Semua <?= count($users); ?> dari total <?= $pelatihan['jumlah_peserta']; ?> Data Registrasi '<?= $pelatihan['nama']; ?>, Tanggal <?= $tglAwal; ?> s/d <?= $tglAkhir; ?>'
    </h2>
    <table border="1">
        <thead>
            <tr style="background-color: crimson; color:white">
                <th scope="col">No</th>
                <th scope="col">Provinsi</th>
                <th scope="col">Instansi</th>
                <th scope="col">Nama</th>
                <th scope="col">L/P</th>
                <th scope="col">ID</th>
                <th scope="col">Tempat Lahir</th>
                <th scope="col">Tanggal Lahir</th>
                <th scope="col">Alamat</th>
                <th scope="col">No HP</th>
                <th scope="col">No WA</th>
                <th scope="col">Email</th>
                <th scope="col">Pendidikan</th>
                <th scope="col">Jurusan</th>
                <th scope="col">Jabatan</th>
                <th scope="col">Bidang</th>
                <th scope="col">Aktif Di Instansi</th>
                <th scope="col">Alamat Instansi</th>
                <th scope="col">No Telp Instansi</th>
                <th scope="col">Email Instansi</th>
                <th scope="col">Biaya</th>
                <th style="width: 200px;" scope="col">Bukti Pembayaran</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            <?php foreach ($users as $user) : ?>
                <tr style="vertical-align: middle;">
                    <th><?= $i++; ?></th>
                    <td><?= $user->prov; ?></td>
                    <td><?= $user->nama_instansi; ?></td>
                    <td><?= $user->namauser; ?></td>
                    <td><?= ($user->jk) == 0 ? "Laki-Laki" : "Perempuan"; ?></td>
                    <td><?= $user->userid; ?></td>
                    <td><?= $user->tempatLhr; ?></td>
                    <td><?= $user->tglLhr; ?></td>
                    <td><?= $user->alamat; ?> <?= $user->kelurahan; ?>, <?= $user->kecamatan; ?>, <?= $user->wilayah; ?>, <?= $user->provinsi; ?>.</td>
                    <td><?= $user->telp2; ?></td>
                    <td><?= $user->telp2; ?></td>
                    <td><?= $user->email; ?></td>
                    <td><?= $user->ijasah; ?></td>
                    <td><?= $user->keilmuan; ?></td>
                    <td><?= $user->jabatan; ?></td>
                    <td><?= $user->bidang; ?></td>
                    <td><?= $user->tglaktif; ?></td>
                    <td><?= $user->alamat_instansi; ?></td>
                    <td><?= $user->telp; ?></td>
                    <td><?= $user->email_instansi; ?></td>
                    <td><?= $user->biaya; ?></td>
                    <td><img src="<?= base_url('/img/bukti-pembayaran/' . $user->bukti_image); ?>"></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>