<?= $this->extend('templates/index'); ?>
<?= $this->section('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Peserta yang sudah Membayar</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <?= view('Myth\Auth\Views\_message_block') ?>
            <div class="row">
                <div class="col-6">
                    <form action="" method="POST">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Masukkan nama" name="keyword">
                            <button class="btn btn-outline-secondary" type="submit" name="submit">Cari</button>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <a class="btn btn-primary float-right" href="<?= base_url('bukti/add/' . $pelatihanid); ?>">+ Tambah Bukti Bayar</a>
                </div>
            </div>
        </div>
        <div class="col">

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Instansi</th>
                        <th scope="col">No. HP</th>
                        <th scope="col">Email</th>
                        <th style="width: 200px;" scope="col">Foto</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1 ?>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <th scope="row"><?= $i++; ?></th>
                            <td><?= $user->namauser; ?></td>
                            <td><?= $user->nama_instansi; ?></td>
                            <td><?= $user->telp2; ?></td>
                            <td><?= $user->email; ?></td>
                            <td><img src="<?= base_url('/img/bukti-pembayaran/' . $user->bukti_image); ?>" class="img-fluid"></td>
                            <td>
                                <a href="<?= base_url('bukti/download/' . $user->buktiid); ?>" class="btn btn-success">Download</a>
                                <a href="<?= base_url('bukti/edit/' . $user->buktiid); ?>" class="btn btn-warning">Edit</a>
                                <a href="<?= base_url('bukti/delete/' . $user->buktiid); ?>" onclick="return confirm('Apakah anda yakin menghapus data?')" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</section>


<?= $this->endSection(); ?>