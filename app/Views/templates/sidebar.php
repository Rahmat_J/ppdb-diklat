<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
        <img src="<?= base_url(); ?>/img/logo/pmi-1.png" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">UDDP PMI</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <?php if (in_groups('admin')) : ?>
                    <li class="nav-header">Admin Management</li>
                    <li class="nav-item">
                        <a href="<?= base_url('pelatihan'); ?>" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                Daftar Pelatihan
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('admin'); ?>" class="nav-link">

                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Daftar Peserta
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('seleksi'); ?>" class="nav-link">
                            <i class="nav-icon fa-solid fa-user-check"></i>
                            <p>
                                Seleksi Peserta
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('rekap'); ?>" class="nav-link">
                            <i class="nav-icon fa-solid fa-user-graduate"></i>
                            <p>
                                Rekap Peserta
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('bukti'); ?>" class="nav-link">
                            <i class="nav-icon fa-solid fa-file-invoice-dollar"></i>
                            <p>
                                Bukti-bukti Pembayaran
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('sertif'); ?>" class="nav-link">
                            <i class="nav-icon fa-solid fa-file-lines"></i>
                            <p>
                                Daftar Sertifikat
                            </p>
                        </a>
                    </li>
                <?php endif; ?>
                <li class="nav-header">User Management</li>
                <li class="nav-item">
                    <a href="/" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('user/profile'); ?>" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            My Profile
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('user/pelatihan'); ?>" class="nav-link">
                        <i class="nav-icon fa-solid fa-book-open"></i>
                        <p>
                            Pilih Pelatihan
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('user/bukti/' . user_id()); ?>" class="nav-link">
                        <i class="nav-icon fa-solid fa-receipt"></i>
                        <p>
                            Upload Bukti Pembayaran
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('user/sertifikat/' . user_id()); ?>" class="nav-link">
                        <i class="nav-icon fa-solid fa-file-pdf"></i>
                        <p>
                            Sertifikat
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>